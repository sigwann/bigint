This C++ project allows to do multi precision arithmetic.

Strong values of this project are:
- Use of operator overloading (high readability)
- Full implementation of negative numbers 
- No use of low level code
- Inline documentation
- No software limit for size of manipulated numbers. Limits will come from your hardware.

Compatibility: C++11 (use of std::regex for instance)

# Bigint

Bigint class defines the big integer object and its methods.

A big integer is a vector of limbs of MAXDIGITS digits.
For instance, when MAXDIGITS is equal to four, integer 12 765 is composed of 2 limbs:
- limb 0: 2765
- limb 1: 1

First limb is the limb at the rightest position.

To init a Bigint:
- Bigint n(15); or Bigint n = 15; for integers
- Bigint n("161163534262"); for strings

Sign is managed: sign = -1 if number is negative, otherwise sign = 1

Operations implemented:
- Addition and Subtraction
- Multiplication (standard or Karatsuba) and Division
- Montgomery reduction
- Fast exponentiation

You can change MAXDIGITS value in Bigint.cpp

# Primality

Primality class provides tools like:
- GCD
- Jacobi symbol
- Lucas sequence
- Fermat, Mersenne primes and Fibonacci number provider

Primality class implements some usual arithmetic tests:
- Miller Rabin
- Pollard p-1
- Pollard Rho Floyd
- Pollard Rho Brent
- Solovay Strassen
- Williams p+1

# Syracuse

Syracuse class allows to do calculations for Syracuse conjecture, aka Ulam conjecture or Collatz conjecture.

# Lychrel

Lychrel class allows to do calculations for palyndrome numbers conjecture

# Known bugs
- randBI() method can't work if 'this' is negative (randomBI or randomValue ??)
- elliptic curve calculations

# ToDo
- Add a way to affect via string Bigint u = "2345"
- Put assert on limb[i] < 0 ? and on (sign==1 || sign==-1) ?
- How to int + Bigint, int * Bigint, int - Bigint
- Constructor per copy, affectation per copy (chapter 18)
- Throw exception on parsing
- Constructor bigint (int x) has no size control: what if x == maxint ?
- Put elliptic curve in static
- Implement destructors
- Put Bigint data in private, add setters/getters
- Put in private or protected, methods that do not need to be in public
- Add namespacing
- Add license
- Track ToDos in source code

# ToDo (new features)
- Barret reduction
- Dixon algo
- Big prime generator
- Discret logarithm
- Fast multiplication fft
- Quadratic sieve
- Sha1
- Sha2
- Full support of base 2

# ToDo (potential optimisations)
- Optimize exponentiationBySquaringModular
- Put most used methods in inline
- Allow to change limb type (int, long, int64, ...)
- sign is a char
- Template int/bigint

# ToDo (tests)
- Test a%b, shiftRight, shiftLeft, abs, read, nbtwopower on 0
- Test operations sequences * * * + / / + / / * - / - ...

//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "Tests.h"
#include "Agent.h"
#include "Primality.h"
#include "Crypto.h"
#include "Syracuse.h"
#include "Lychrel.h"

uint32_t Tests::testIndex = 1;

bool Tests::TestParsing()
{
  cout << "-------------\nTests Parsing\n-------------\n" << endl;
  bool TestsResult = true;

  vector<pair<string, int>> tests {{"0", 0}, {"+1", 1}, {"-1", -1},
    {"ABCD", 0}, {"12C3", 12}, {"-12C3", -12},
    {"+12.3", 12}, {"--34", -34}, {"125 6 89 65", 12568965},
    {"1234", 1234}, {"68965", 68965}, {"3*7", 3}};
  
  for (const auto& test : tests)
  {
    Bigint t(test.first);
    cout << "Test " << testIndex++ << ": Parse string " << test.first << ". After parsing: " << t << endl;
    TestsResult &= displayResult(t == test.second);
  }
  {
    Bigint test("");
    cout << "Test " << testIndex++ << ": Parse empty" << ". After parsing: " << test << endl;
    TestsResult &= displayResult(test == 0);
  }
  {
    Bigint test("61");
    cout << "Test " << testIndex++ << ": to_int 61" << ". To int: " << test.to_int() << endl;
    TestsResult &= displayResult(test.to_int() == 61);
  }
  {
    Bigint test("178261");
    cout << "Test " << testIndex++ << ": to_int 178261" << ". To int: " << test.to_int() << endl;
    TestsResult &= displayResult(test.to_int() == 178261);
  }

  /* Uncomment to test int -> Bigint -> int
  for (int i = 0; i < Bigint::LIMB_MAX * Bigint::LIMB_MAX; ++i)
  {
    Bigint test(i);
    //Bigint test = i;
    assert(test.to_int() == i);
  }*/
  
  return displayResult(TestsResult, "s Parsing:");
}

bool Tests::TestDisplay()
{
  cout << "-------------\nTests Display\n-------------\n" << endl;
  bool TestsResult = true;

  cout << "Test " << testIndex++ << ": display 0" << endl;
  Bigint test = 0;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.toString().length() == 1);

  cout << "Test " << testIndex++ << ": display -1" << endl;
  test = -1;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.toString().length() == 2);

  cout << "Test " << testIndex++ << ": display -986" << endl;
  test = -986;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.toString().length() == 4);

  cout << "Test " << testIndex++ << ": display 17" << endl;
  test = 17;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.toString().length() == 2);
  
  cout << "Test " << testIndex++ << ": display 3157" << endl;
  test = 3157;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.toString().length() == 4);
  
  cout << "Test " << testIndex++ << ": display 43157" << endl;
  test = 43157;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.toString().length() == 5);
  
  cout << "Test " << testIndex++ << ": display big number" << endl;
  
  Bigint test5("1 699 561 964 616 384 161 338 617 241 266 196 262 188 138 717 953 664 347 032 399 264 096 935 675 908 809 541 428 428 471 476 937 555 523 991 706 728 027 766 774 612 668 679 450 953 189 201 786 691 147 017 727 293 157 388 178 713 416 872 419 209 575 445 730 542 665 482 540 199 328 542 591 700 483 611 092 571 869 723 366 714 895 009 987 737 984 407 155 328 706 172 308 072 586 587 069 702 822 663 252 926 493 284 050 976 621 964 269 866 309 882 547 924 042 142 640 211 772 475 321 604 934 602 253 555 678 145 254 948 026 105 110 728 239 023 964 268 214 068 143 794 141 333 406 816 042 259 537 749 579 103 841 338 918 524 480 066 021 879 602 734 021 704 978 809 402 291 701 560 788 959 616 074 777 136 719 685 733 408 168 123 290 607 791 830 139 617 258 517 667 225 573 775 362 509 000 410 810 649 096 182 973 976 388 931 647 864 297 491 176 457 086 470 420 122 129 855 548 772 364 197 416 197 649 796 186 249 342 956 347 336 559 024 651 463 204 755 158 369 511 629 516 068 231 621 505 604 025 139 693 345 243 108 755 456 290 442 704 280 265 942 567 830 119 443 696 615 946 894 227 840 943 489 640 677 553 921 130 195 067 357 577 142 000 537 843 818 248 942 845 020 279 428 511 017 756 531 930 125 176 058 405 380 485 542 601 667 058 129 037 930 864 191 730 978 307 717 341 352 897 756 289 383 591 739 804 033 426 723 777 719 670 445 527 149 182 902 489 228 327 827 479 723 548 261 955 413 994 940 538 021 529 745 642 990 381 064 092 692 418 236 991 150 773 353 808 374 939 787 881 913 123 010 148 364 632 635 054 938 161 036 183 837 681 822 863 606 366 947 992 914 855 017 527 115 516 758 890 926 031 622 693 581 839 854 653 881 955 480 637 371 901 943 440 891 838 621 025 947 888 794 867 955 278 791 029 164 761 748 289 155 337 484 844 989 655 410 177 798 223 045 238 508 891 866 521 569 410 416 178 264 305 971 207 094 275 757 440 398 092 136 552 743 009 268 167 697 719 171 030 648 658 668 991 814 465 539 564 873 338 441 641 613 495 800 978 575 570 020 342 092 706 384 288 261 832 797 208 328 337 778 994 466 834 465 472 570 779 046 503 212 913 611 298 370 650 584 022 579 462 422 353 165 158 230 798 076 904 889 935 375 127 006 857 369 897 377 471 441 694 542 130 227 881 829 294 346 959 706 726 373 666 074 839 119 742 540 857 442 888 945 739 350 033 976 931 450 766 631 373 317 554 939 538 712 123 303 120 270 575 548 003 246 477 880 450 875 433 484 198 314 592 759 419 056 816 006 877 440 170 648 974 131 308 742 757 289 492 441 439 255 742 276 554 981 415 258 461 314 737 767 724 176 910 157 699 603 312 368 442 176 406 808 498 446 214 185 879 513 118 250 092 456 450 055 254 784 750 091 387 102 752 887 521 058 303 538 887 337 261 111 638 814 476 319 279 143 886 756 327 937 855 458 979 033 619 714 854 444 428 416 496 373 120 347 583 578 597 732 466 442 448 759 281 343 060 490 260 916 539 053 054 640 462 347 909 947 031 833 700 502 843 009 352 450 637 573 123 564 222 201 929 167 330 363 796 978 886 790 520 409 028 227 380 527 433 525 026 878 144 681 751 017 786 456 437 933 280 933 581 370 828 713 894 037 918 877 297 572 184 754 434 002 055 750 741 344 593 992 294 127 160 487 028 664 692 046 339 706 749 778 606 997 473 493 475 624 989 258 307 521 295 628 319 202 985 302 905 841 313 058 545 692 825 225 040 087 198 101 386 372 316 911 563 069 560 664 575 112 674 578 311 370 439 628 131 299 425 906 246 940 023 617 674 557 898 732 524 189 417 614 048 791 990 054 017 074 659 673 384 791 214 061 557 438 806 277 497 840 853 238 477 150 127 064 070 356 946 464 125 726 127 584 317 635 111 241 772 449 516 371 288 168 537 925 276 740 007 191 950 151 903 909 772 235 756 302 161 226 003 879 372 270 399 598 214 688 313 096 886 154 119 632 808 418 433 891 532 304 756 649 259 664 309 221 267 239 957 567 283 590 076 417 144 878 616 942 052 083 363 989 807 418 150 550 142 685 570 890 595 245 470 371 151 674 220 619 193 046 520 208 095 043 255 212 127 247 105 490 096 234 776 572 000 195 958 524 294 407 340 893 432 431 942 061 573 804 826 803 706 498 821 354 834 961 568 552 871 259 568 895 296 030 576 625 727 132 087 482 070 234 570 444 312 712 175 863 548 402 364 202 283 602 979 110 708 534 168 804 462 223 618 806 406 024 123 532 557 025 361 963 499 971 791 002 521 318 636 163 633 455 550 630 874 023 853 307 738 080 188 562 417 322 721 984 144 378 887 351 965 221 728 452 778 227 911 809 612 011 486 828 512 395 588 556 267 060 800 344 288 335 281 705 013 636 723 337 930 164 243 355 384 551 226 554 248 075 451 438 961 387 399 333 599 300 874 600 241 801 322 386 644 259 293 096 967 072 475 655 734 273 358 896 025 328 991 115 379 150 926 778 351 558 958 865 312 155 721 475 481 309 843 509 142 379 256 491 940 580 607 589 890 913 692 402 797 207 529 680 369 029 100 800 844 603 280 986 385 819 695 333 261 466 862 876 854 438 699 974 464 442 429 139 800 504 695 075 579 036 159 820 593 909 725 317 825 100 941 975 781 270 652 828 764 565 491 931 659 037 591 480 194 030 162 086 675 157 814 358 538 613 101 137 446 432 474 679 608 876 041 159 858 662 352 018 955 258 614 103 053 755 742 441 615 992 436 443 248 726 641 033 297 951 300 716 370 702 557 410 178 968 335 583 331 067 943 274 949 574 482 311 848 248 646 299 751 235 726 251 501 136 053 797 544 533 950 079 612 775 636 858 859 326 522 318 088 609 165 080 784 350 535 499 949 129 557 442 420 942 319 794 569 952 166 319 943 426 089 898");
  cout << test5.toString() << endl;
  cout << "nb digits: " << test5.nbDigits() << endl;
  TestsResult &= displayResult(test5.toString().length() == 3988);
  
  // flight delay: 100987

  cout << "Test " << testIndex++ << ": display big number" << endl;

  Bigint test6("911 093 475 353 657 403 974 270 169 110 636 359 727 823 814 852 171 098 911 393 708 844 023 784 573 633 330 658 193 173 665 116 776 752 934 132 070 160 381 092 265 838 726 682 436 416 427 367 787 667 219 244 965 970 212 869 429 582 753 377 470 716 730 666 242 289 644 000 870 593 426 824 899 986 133 439 926 374 256 436 859 603 480 480 809 410 455 419 949 541 483 240 927 869 498 212 597 873 082 311 592 124 588 162 023 479 131 340 533 890 500 122 238 860 186 517 296 756 579 979 473 763 829 218 629 179 202 148 440 341 420 305 077 709 812 854 173 336 750 037 739 742 483 052 180 001 630 138 417 009 269 331 658 749 554 346 766 379 781 106 195 507 879 679 413 328 931 245 811 501 146 686 272 448 930 027 662 940 420 418 758 475 829 754 686 914 413 728 611 425 650 116 746 941 781 462 625 242 144 629 101 120 331 431 179 587 241 519 021 084 555 988 892 821 058 465 898 151 040 999 670 241 520 534 564 588 550 189 437 146 945 891 717 990 964 382 666 212 950 109 431 680 450 422 016 964 611 810 572 786 994 411 358 519 486 855 383 648 446 336 073 746 290 590 188 210 070 934 704 726 855 207 621 263 171 577 809 222 812 040 640 132 423 019 671 705 134 745 329 071 386 526 114 124 842 502 823 680 194 752 847 158 238 432 142 780 790 870 041 958 201 348 079 404 613 436 629 249 326 561 564 728 055 140 350 022 755 109 185 775 827 454 993 464 837 313 757 753 142 838 551 708 786 409 526 640 066 673 984 545 691 957 469 820 538 860 157 029 824 739 900 854 216 745 985 617 742 903 666 958 690 993 561 391 655 420 100 779 188 693 353 793 373 017 933 520 200 898 689 655 555 593 568 818 982 863 595 902 730 205 767 537 071 441 100 763 167 438 810 847 346 430 477 920 356 086 653 516 403 154 276 518 492 671 502 237 990 769 323 632 820 575 811 565 636 155 770 631 081 213 553 818 341 436 752 374 496 225 610 138 983 571 204 058 015 792 736 875 956 727 924 388 582 145 893 659 483 233 351 668 067 429 718 281 559 752 249 599 012 796 119 383 165 867 334 067 094 264 157 573 024 548 319 864 783 880 780 400 338 590 105 848 724 683 675 232 783 257 595 606 831 892 877 025 405 081 619 647 995 611 995 304 574 945 512 380 758 571 844 203 801 692 696 807 922 403 096 864 791 519 613 350 622 286 266 528 615 084 082 573 432 425 319 821 093 859 017 788 848 531 644 657 206 639 273 378 825 705 413 093 673 507 632 552 441 051 503 555 281 191 787 342 796 450 277 630 740 615 831 363 525 469 490 154 053 311 488 428 600 217 891 843 947 441 216 485 094 568 870 120 278 535 389 796 811 089 410 290 931 568 712 379 114 041 066 223 634 409 140 015 593 570 927 584 393 623 342 950 413 175 359 216 674 581 436 100 747 977 099 979 962 846 688 729 286 930 822 439 856 265 626 722 586 250 540 626 055 859 444 671 673 174 382 309 501 006 945 674 996 918 004 281 970 944 744 697 349 175 346 423 826 993 932 673 115 229 760 597 009 527 791 913 557 634 820 245 566 518 747 576 087 493 990 840 066 730 387 466 334 194 088 043 819 734 629 860 175 890 279 360 624 558 979 234 128 142 749 971 417 529 323 551 375 387 718 232 615 167 189 046 928 410 532 160 862 494 648 449 774 944 606 902 244 283 905 442 558 395 089 988 455 638 700 384 612 236 707 477 429 251 159 412 596 373 229 003 386 494 829 004 990 523 529 428 447 370 322 887 616 765 247 526 494 507 843 275 912 546 682 201 249 986 231 344 936 121 320 065 162 159 627 352 126 103 209 597 111 403 786 581 518 297 601 258 169 508 127 339 580 092 617 736 919 162 583 560 104 747 772 134 985 292 130 419 241 411 986 735 055 549 646 886 777 384 902 088 258 793 835 958 059 764 191 371 792 177 168 320 439 732 742 160 740 992 224 391 356 255 286 196 720 080 134 049 376 961 025 220 104 851 946 691 806 863 003 150 907 791 673 490 672 217 634 081 915 865 065 262 264 811 378 716 757 027 246 404 257 719 381 611 392 436 180 082 750 236 138 356 587 603 710 408 423 275 966 688 074 306 770 341 086 378 600 032 028 696 640 800 362 910 854 054 596 012 205 597 838 014 819 295 939 673 714 236 992 863 799 605 387 257 802 429 495 268 369 873 872 305 705 654 563 185 184 443 124 340 873 405 110 570 055 653 517 196 594 401 991 167 026 405 399 071 650 560 247 752 562 425 739 665 171 066 034 244 783 452 631 708 578 945 364 859 493 920 268 441 462 930 100 941 155 031 258 817 567 082 245 146 614 475 918 844 862 528 603 999 433 789 233 262 350 512 894 673 753 650 618 607 885 435 049 733 208 865 378 589 924 778 819 647 340 805 462 833 086 006 617 304 619 152 296 889 256 859 832 774 070 433 712 333 702 561 481 764 343 929 075 183 006 105 042 021 333 053 257 851 070 485 096 484 073 332 261 924 212 049 485 822 161 909 961 312 614 768 228 920 578 700 826 894 955 186 552 357 351 826 654 937 226 600 742 214 628 149 996 531 421 602 996 149 254 997 284 095 360 726 887 541 408 546 072 757 389 370 707 716 607 068 082 436 108 938 916 352 391 583 749 981 676 325 403 321 495 083 362 786 075 592 587 218 970 261 178 566 867 267 691 944 402 026 064 263 901 863 413 922 162 267 299 281 588 980 704 437 513 318 063 002 959 551 358 062 107 187 113 287 395 009 399 689 925 046 927 867 633 819 168 614 788 941 354 367 909 398 808 763 574 541 406 745 185 352 949 271 222 616 586 324 492 618 546 716 357 248 611 037 494 188 691 962 315 491 826 556 805 035 560 891 376 070 594 046 525 181 493 580 624 055 653 069 723 155 084 697 467 978 961 349 365 919 704 580 080 603 267 601 101 329 581 635 270 621 128 229 978 890 791 717 316 334 365 382");
  cout << test6.toString() << endl;
  cout << "nb digits: " << test6.nbDigits() << endl;
  TestsResult &= displayResult(test6.toString().length() == 3996);

  // flight delay: 100473
  
  cout << "Test " << testIndex++ << ": display -1573" << endl;
  test = -1573;
  cout << test.toString() << "\nformatted: " << test << endl;
  TestsResult &= displayResult(test.nbDigits() == 4 && test.toString() == "-1573");
  
  return displayResult(TestsResult, "s Display:");
}

bool Tests::TestDigits()
{
  cout << "------------\nTests Digits\n------------\n" << endl;
  bool TestsResult = true;
  
  cout << "Test " << testIndex++ << ": nb of digits for 0 = 1" << endl;
  TestsResult &= displayResult(Bigint(0).nbDigits() == 1);

  cout << "Test " << testIndex++ << ": nb of digits for 721 = 3" << endl;
  TestsResult &= displayResult(Bigint(721).nbDigits() == 3);

  cout << "Test " << testIndex++ << ": nb of digits for 91294 = 5" << endl;
  TestsResult &= displayResult(Bigint(91294).nbDigits() == 5);

  cout << "Test " << testIndex++ << ": nb of digits for -32 = 2" << endl;
  TestsResult &= displayResult(Bigint(-32).nbDigits() == 2);

  cout << "Test " << testIndex++ << ": sum of digits for 721 = 10" << endl;
  TestsResult &= displayResult(Bigint(721).sumOfDigits() == 10);

  cout << "Test " << testIndex++ << ": sum of digits for 111111111 = 9" << endl;
  TestsResult &= displayResult(Bigint(111111111).sumOfDigits() == 9);

  cout << "Test " << testIndex++ << ": sum of digits for 999999 = 54" << endl;
  TestsResult &= displayResult(Bigint(999999).sumOfDigits() == 54);

  cout << "Test " << testIndex++ << ": sum of digits for 1020304 = 10" << endl;
  TestsResult &= displayResult(Bigint(1020304).sumOfDigits() == 10);

  return displayResult(TestsResult, "s Digits:");
}

bool Tests::TestOperators()
{
  cout << "---------------\nTests Operators\n---------------\n" << endl;
  bool TestsResult = true;
  
  Bigint a(1);
  Bigint b(0);
  Bigint c;
  
  cout << "Test " << testIndex++ << ": a = 331221, c = -a (test unitary operator)" << endl;
  a = 331221;
  c = -a;
  TestsResult &= displayResult(c == -331221);

  vector<tuple<Bigint, int, Bigint>> tests_plusInt {
    {  0,   0,    0},
    {  3,   2,    5},
    {  7,   0,    7},
    {  7, -12,   -5},
    { -7, -12,  -19},
    { -7,  12,    5},
    {231, 769, 1000}};
  for (const auto& test : tests_plusInt)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " + (" << get<1>(test) << ") = " << get<2>(test) << " (Bigint + int)" << endl;
    TestsResult &= displayResult(get<0>(test) + get<1>(test) == get<2>(test));
  }

  vector<tuple<Bigint, int, Bigint>> tests_minusInt {
    { 0,   0,   0},
    { 3,   2,   1},
    { 7,   0,   7},
    { 7, -12,  19},
    {-7, -12,   5},
    {-7,  12, -19}};
  for (const auto& test : tests_minusInt)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " - (" << get<1>(test) << ") = " << get<2>(test) << " (Bigint - int)" << endl;
    TestsResult &= displayResult(get<0>(test) - get<1>(test) == get<2>(test));
  }

  vector<tuple<Bigint, Bigint, Bigint>> tests_plusBigint {
    {     0,      0,       0},
    {     0,      1,       1},
    {     3,      2,       5},
    {     5,      7,      12},
    {     7,      0,       7},
    {     7,    -12,      -5},
    {    -7,    -12,     -19},
    {    -7,     12,       5},
    {   231,    769,    1000},
    {331221, 113454,  444675},
    {345678, 234567,  580245},
    {999999,      1, 1000000}};
  for (const auto& test : tests_plusBigint)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " + (" << get<1>(test) << ") = " << get<2>(test) << " (Bigint + Bigint)" << endl;
    TestsResult &= displayResult(get<0>(test) + get<1>(test) == get<2>(test));
  }

  vector<tuple<Bigint, Bigint, Bigint>> tests_minusBigint {
    {     0,      0,      0},
    {     3,      2,      1},
    {     5,      7,     -2},
    {     7,      0,      7},
    {     7,    -12,     19},
    {    -7,    -12,      5},
    {    -7,     12,    -19},
    {331221, 113454, 217767},
    {345678, 234567, 111111}};
  for (const auto& test : tests_minusBigint)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " - (" << get<1>(test) << ") = " << get<2>(test) << " (Bigint - Bigint)" << endl;
    TestsResult &= displayResult(get<0>(test) - get<1>(test) == get<2>(test));
  }

  vector<pair<Bigint, Bigint>> tests_inc {
    {       0,        1},
    {      -1,        0},
    {      -3,       -2},
    {    9999,    10000},
    {99999999,100000000},
    {  -10000,    -9999}};
  for (auto& test : tests_inc)
  {
    cout << "Test " << testIndex++ << ": (" << test.first << ")++ = " << test.second << endl;
    TestsResult &= displayResult(test.first++ == test.second);
  }
  
  vector<pair<Bigint, Bigint>> tests_dec {
    {        0,       -1},
    {        1,        0},
    {       -3,       -4},
    {    10000,     9999},
    {100000000, 99999999},
    {    -9999,   -10000}};
  for (auto& test : tests_dec)
  {
    cout << "Test " << testIndex++ << ": (" << test.first << ")-- = " << test.second << endl;
    TestsResult &= displayResult(test.first-- == test.second);
  }

  vector<tuple<Bigint, Bigint, Bigint>> tests_mulBigint {
    {   0,   0,      0},
    {   3,   2,      6},
    {   7,   0,      0},
    {   3,  -7,    -21},
    {  -9, -11,     99},
    { -11,   5,    -55},
    {1221,3454,4217334}};
  for (const auto& test : tests_mulBigint)
  {
    cout << "Test " << testIndex++ << ": (" << get<0>(test) << ") * (" << get<1>(test) << ") = " << get<2>(test) << " (Bigint * Bigint)" << endl;
    TestsResult &= displayResult(get<0>(test) * get<1>(test) == get<2>(test));
  }

  vector<tuple<Bigint, int, Bigint>> tests_mulInt {
    {   0,   0,      0},
    {   3,   2,      6},
    {   7,   0,      0},
    {   3,  -7,    -21},
    {  -9, -11,     99},
    { -11,   5,    -55},
    {1221,3454,4217334}};
  for (const auto& test : tests_mulInt)
  {
    cout << "Test " << testIndex++ << ": (" << get<0>(test) << ") * (" << get<1>(test) << ") = " << get<2>(test) << " (Bigint * int)" << endl;
    TestsResult &= displayResult(get<0>(test) * get<1>(test) == get<2>(test));
  }

  cout << "Test " << testIndex++ << ": a = 11 then a += 5 (5 as big int)" << endl;
  a = 11;
  b = 5;
  a +=b;
  TestsResult &= displayResult(a == 16);

  cout << "Test " << testIndex++ << ": a = 11 then a += -5 (-5 as big int)" << endl;
  a = 11;
  b = -5;
  a +=b;
  TestsResult &= displayResult(a == 6);
  
  cout << "Test " << testIndex++ << ": a = -11 then a += 5 (5 as big int)" << endl;
  a = -11;
  b = 5;
  a +=b;
  TestsResult &= displayResult(a == -6);
  
  cout << "Test " << testIndex++ << ": a = -11 then a += -5 (-5 as big int)" << endl;
  a = -11;
  b = -5;
  a +=b;
  TestsResult &= displayResult(a == -16);
  
  cout << "Test " << testIndex++ << ": a = 11 then a -= 5 (5 as big int)" << endl;
  a = 11;
  b = 5;
  a -=b;
  TestsResult &= displayResult(a == 6);
  
  cout << "Test " << testIndex++ << ": a = 11 then a -= -5 (-5 as big int)" << endl;
  a = 11;
  b = -5;
  a -=b;
  TestsResult &= displayResult(a == 16);
  
  cout << "Test " << testIndex++ << ": a = -11 111 then a *= 5 (5 as big int)" << endl;
  a = -11111;
  b = 5;
  a *=b;
  TestsResult &= displayResult(a == -55555);
  
  cout << "Test " << testIndex++ << ": a = -11 111 then a *= 5 (5 as int)" << endl;
  a = -11111;
  int d = 5;
  a *=d;
  TestsResult &= displayResult(a == -55555);

  cout << "Test " << testIndex++ << ": a = 11 111 then a *= -5 (-5 as big int)" << endl;
  a = 11111;
  b = -5;
  a *=b;
  TestsResult &= displayResult(a == -55555);
  
  cout << "Test " << testIndex++ << ": a = 11 111 then a *= -5 (-5 as int)" << endl;
  a = 11111;
  d = -5;
  a *= d;
  TestsResult &= displayResult(a == -55555);

  cout << "Test " << testIndex++ << ": a = 16 211 then a *= 0 (0 as big int)" << endl;
  a = 16211;
  b = 0;
  a *= b;
  TestsResult &= displayResult(a == 0);
  
  cout << "Test " << testIndex++ << ": a = 16 211 then a *= 0 (0 as int)" << endl;
  a = 16211;
  d = 0;
  a *= d;
  TestsResult &= displayResult(a == 0);

  vector<tuple<Bigint, Bigint, Bigint>> tests_mod {
    {   10,  5,  0},
    {11111, 17, 10},
    {   27,  0, 27},
    {  -27,  7,  1},
    {   27, -7, -1},
    {  -27, -7, -6},
    {-1208,  Bigint("102 122 068 899 379"),  Bigint("102 122 068 898 171")},
    { 1208, Bigint("-102 122 068 899 379"), Bigint("-102 122 068 898 171")}};
  for (const auto& test : tests_mod)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " % " << get<1>(test) << " = " << get<2>(test) << endl;
    TestsResult &= displayResult(get<0>(test) % get<1>(test) == get<2>(test));
  }

  /*cout << "Test " << testIndex++ << ": 1598302720/3" << endl;
  Bigint q;
  Bigint r;
  Bigint bug("1598302720");
  bug.division(3, q, r);
  TestsResult &= displayResult(r == 1);*/
  
  cout << "Test " << testIndex++ << ": inverse 13[5] is 2" << endl;
  a = 13;
  b = 5;
  c = a.extendedEuclide(b)[0];
  TestsResult &= displayResult(c == 2);

  cout << "Test " << testIndex++ << ": inverse 23 453[5 819] is 5 556" << endl;
  a = 23453;
  b = 5819;
  c = a.extendedEuclide(b)[0];
  TestsResult &= displayResult(c == 5556);

  cout << "Test " << testIndex++ << ": inverse 12[18] does not exist" << endl;
  a = 12;
  b = 18;
  TestsResult &= displayResult(a.extendedEuclide(b).empty());

  vector<tuple<Bigint, int, Bigint>> tests_powInt {
    { 0, 0,   1},
    { 0, 3,   0},
    { 3, 6, 729},
    {-3, 5,-243}};
  for (const auto& test : tests_powInt)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " ^ " << get<1>(test) << " = " << get<2>(test) << endl;
    TestsResult &= displayResult((get<0>(test) ^ get<1>(test)) == get<2>(test));
  }

  vector<tuple<Bigint, Bigint, Bigint>> tests_powBigint {
    { 0,  0, Bigint("1")},
    {-3, 15, Bigint("-14 348 907")},
    { 3, 37, Bigint("450 283 905 890 997 363")}};
  for (const auto& test : tests_powBigint)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " ^ " << get<1>(test) << " = " << get<2>(test) << endl;
    TestsResult &= displayResult((get<0>(test) ^ get<1>(test)) == get<2>(test));
  }
  
  cout << "Test " << testIndex++ << ": 3^37 modulo 51 = 39" << endl;
  a = 3;
  b = a.exponentiationBySquaringModular(37, 51);
  TestsResult &= displayResult(b == 39);
  
  cout << "Test " << testIndex++ << ": 4444^4444 modulo 9 = 7" << endl;
  a = 4444;
  b = a.exponentiationBySquaringModular(4444, 9);
  TestsResult &= displayResult(b == 7);
  
  cout << "Test " << testIndex++ << ": 3^3 modulo 1 = 27" << endl;
  a = 3;
  b = a.exponentiationBySquaringModular(3, 1);
  TestsResult &= displayResult(b == 27);
  
  cout << "Test " << testIndex++ << ": 2^8 modulo 1 = 256" << endl;
  a = 2;
  b = a.exponentiationBySquaringModular(8, 1);
  TestsResult &= displayResult(b == 256);
  
  cout << "Test " << testIndex++ << ": number of 2 powers in 6 = 1" << endl;
  a = 6;
  int nbTwo = a.nbTwoPower();
  TestsResult &= displayResult(nbTwo == 1);
  
  cout << "Test " << testIndex++ << ": number of 2 powers in 48 = 4" << endl;
  a = 48;
  nbTwo = a.nbTwoPower();
  TestsResult &= displayResult(nbTwo == 4);
  
  cout << "Test " << testIndex++ << ": number of 2 powers in 27 = 0" << endl;
  a = 27;
  nbTwo = a.nbTwoPower();
  TestsResult &= displayResult(nbTwo == 0);

  cout << "Test " << testIndex++ << ": 3 * 5 * 2 - 7 + 6 = 29" << endl;
  a = 3;
  b = 7;
  c = a * 5 * 2 + 0 - b + 6;
  TestsResult &= displayResult(c == 29);

  cout << "Test " << testIndex++ << ": 69 251 700 * 310 553 863 658 = 21 506 382 999 884 718 600" << endl;
  a = 69251700;
  b = Bigint("310 553 863 658");
  c = a * b;
  TestsResult &= displayResult(c == Bigint("21 506 382 999 884 718 600"));

  vector<pair<string, string>> tests_mul {{"3 348 055 334 743 600 254 342 761 950 789 909 571 483 090 464 904 053 067 492 614 351 548 620 476 203 113 807 045 334 639 614 815 516 264 166 582 630 491 832 387 936 484 718 987 265 586 414 635 381 320 672 016 704", "9 960 943 274 813 671 003 290 419 704 110 820 692 702 654 418 347 119 683 684 955 961 452 282 204 224 694 671 270 071 192 165 730 669 032 969 720 844 653 567 448 142 942 739 587 194 882 796 007 121 400 208 729 527 863 899 054 645"}, {"643 615 812 036 909 579 016 329 832 064 550 542 111 427 686 629 262 639 444 079 947 893 378 113 815 746 759 352 245 080 959 878 456 323 584 302 997 639 496 759 851 835 290 751 678 820 104 187 583 822 632 689 206 432 201 831", "362 501 660 931 611 676 603 424 479 559 092 064 204 315 037 794 776 467 770 340 609 154 676 071 898 359 217 315 725 037 810 146 113 223 782 350 541 612 082 477 119 967 200 045 718 894 398 710 499 893 750 913 451 373 973 877"}};
  
  for (const auto& test : tests_mul)
  {
    cout << "Test " << testIndex++ << ": Karatsuba" << endl;
    Bigint test1(test.first);
    Bigint test2(test.second);
    TestsResult &= displayResult(test1.standardMultiplication(test2) == test1.karatsubaMultiplication(test2));
  }
  
  {
    cout << "Test " << testIndex++ << ": Montgomery reduction 563 x 190^{-1} [187] = 63" << endl;
    
    Bigint T(563);
    Bigint R(190);
    Bigint Modulus(187);
    auto ee = R.extendedEuclide(Modulus);
    Bigint R_inverse = ee[0];
    Bigint Modulus_opposite_inverse = -ee[1];
    
    TestsResult &= displayResult(T.MontgomeryReduction(R, R_inverse, Modulus, Modulus_opposite_inverse) == (T * R_inverse) % Modulus);
  }
  
  for (auto test : Lychrel::lychrelTable)
  {
    cout << "Test " << testIndex++ << ": Lychrel number " << get<0>(test) << " -> " << get<1>(test) << endl;
    int nbSteps = 0;
    bool abort = false;
    TestsResult &= displayResult(Lychrel::palindrome(get<0>(test), nbSteps, abort) == get<1>(test) && nbSteps == get<2>(test));
  }
  //Lychrel::explore(0, 3000, false, false);
  Lychrel::findFirstNumberGivenNbStep(Bigint(700269));
  
  return displayResult(TestsResult, "s Operators:");
}

bool Tests::TestInequalities()
{
  cout << "------------------\nTests Inequalities\n------------------\n" << endl;
  bool TestsResult = true;
  
  int index = 1;
  vector<tuple<Bigint, Bigint, bool>> tests_lt {
    {  0,   0, false},
    { -0,   0, false},
    { -1,  -1, false},
    {  1,   1, false},
    { -1,   0, true},
    {  0,   1, true},
    { -1,   1, true},
    { 17, Bigint("435262728"), true},
    {-18, 132, true}};
  for (const auto& test : tests_lt)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " < " << get<1>(test) << " is " << boolalpha << get<2>(test) << endl;
    TestsResult &= displayResult(get<0>(test) < get<1>(test) == get<2>(test));
    index++;
    
    if (index < 6)
      continue;
    
    cout << "Test " << testIndex++ << ": " << get<1>(test) << " < " << get<0>(test) << " is " << boolalpha << !get<2>(test) << endl;
    TestsResult &= displayResult(get<1>(test) < get<0>(test) == !get<2>(test));
  }
  
  index = 1;
  vector<tuple<Bigint, Bigint, bool>> tests_gt {
    {  0,   0, false},
    { -0,   0, false},
    { -1,  -1, false},
    {  1,   1, false},
    { -1,   0, false},
    {  0,   1, false},
    { -1,   1, false},
    { 17, Bigint("435262728"), false},
    {-18, 132, false}};
  for (const auto& test : tests_gt)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " > " << get<1>(test) << " is " << boolalpha << get<2>(test) << endl;
    TestsResult &= displayResult(get<0>(test) > get<1>(test) == get<2>(test));
    index++;
    
    if (index < 6)
      continue;
    
    cout << "Test " << testIndex++ << ": " << get<1>(test) << " > " << get<0>(test) << " is " << boolalpha << !get<2>(test) << endl;
    TestsResult &= displayResult(get<1>(test) > get<0>(test) == !get<2>(test));
  }

  vector<tuple<Bigint, Bigint, bool>> tests_leq {
    {   0,     0, true},
    {  -0,     0, true},
    {   1,     1, true},
    {  -1,    -1, true},
    {  -1,    -7, false},
    {5276,  5726, true},
    {-986, 34517, true}};
  for (const auto& test : tests_leq)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " <= " << get<1>(test) << " is " << boolalpha << get<2>(test) << endl;
    TestsResult &= displayResult(get<0>(test) <= get<1>(test) == get<2>(test));
  }

  vector<tuple<Bigint, Bigint, bool>> tests_geq {
    {   0,   0, true},
    {  -0,   0, true},
    {   1,   1, true},
    {  -1,  -1, true},
    {  -1,  -7, true},
    {1345, -74, true},
    {Bigint("12345675555"), Bigint("123456789"), true}
  };
  for (const auto& test : tests_geq)
  {
    cout << "Test " << testIndex++ << ": " << get<0>(test) << " >= " << get<1>(test) << " is " << boolalpha << get<2>(test) << endl;
    TestsResult &= displayResult(get<0>(test) >= get<1>(test) == get<2>(test));
  }
  
  return displayResult(TestsResult, "s Inequalities:");
}

bool Tests::TestStress()
{
  Bigint a;
  Bigint b;
  Bigint c;
  int n;
  
  a.randBI(1, 4);
  b.randBI(1, 4);
  
  for (int i = 0; i < 100000; ++i)
  {
    int type = rand() % 9;
    
    cout << "#" << i << ": " << a << ",  " << b << ", type=" << type << endl;
    
    switch (type)
    {
      case 0:
        a = a + b;
        break;
      case 1:
        a = a - b;
        break;
      case 2:
        a = a * b;
        break;
      case 3:
        a = -a;
        break;
      case 4:
        a++;
        break;
      case 5:
        a--;
        break;
      case 6:
        a += b;
        break;
      case 7:
        a -= b;
        break;
      case 8:
        a = a % b;
        break;
      case 9:
        a %= b;
        break;
    }
    
    b.randBI(1, 4);
    
    c = b.randomValue();
    
    n = b.nbTwoPower();
    
    b.mSign = (rand() % 2) == 0 ? 1 : -1;
  }
  
  return true;
}

bool Tests::TestPrimality()
{
  cout << "---------------\nTests Primality\n---------------\n" << endl;
  bool TestsResult = true;
  
  cout << "Test " << testIndex++ << ": first 50 primes" << endl;
  TestsResult &= displayResult(Primality::firstPrimesTable(50) == Primality::primesTable);

  vector<tuple<int, int, int>> tests_GCD {
    {  123450,   10784,     2},
    {     357,     561,    51},
    {28851538, 1183019, 17657}};
  for (const auto& test : tests_GCD)
  {
    Bigint a(get<0>(test));
    Bigint b(get<1>(test));
    cout << "Test " << testIndex++ << ": GCD(" << a <<", " << b << ") = " << get<2>(test) << endl;
    TestsResult &= displayResult(Primality::GCD(a, b) == get<2>(test));
  }

  vector<pair<int, bool>> tests_Prime {{0, false}, {1, false}, {2, true}, {7, true}, {8, false}, {19, true}, {163, true}};
  for (const auto& test : tests_Prime)
  {
    Bigint a(test.first);
    cout << "Test " << testIndex++ << ": isPrime(" << a << ") = " << boolalpha << test.second << endl;
    TestsResult &= displayResult(Primality::isPrime(a) == test.second);
  }
  for (int i = 0; i < 5; ++i)
  {
    auto a = Primality::FermatNumber(i);
    cout << "Test " << testIndex++ << ": isPrime(" << a << ") = true" << endl;
    TestsResult &= displayResult(Primality::isPrime(a) == true);
  }
  for (int i = 1; i < 6; ++i)
  {
    auto a = Primality::MersennePrimeNumber(i);
    cout << "Test " << testIndex++ << ": isPrime(" << a << ") = true" << endl;
    TestsResult &= displayResult(Primality::isPrime(a) == true);
  }

  vector<pair<Bigint, Bigint>> tests_firstFactor {{4, 2}, {6, 2}, {15, 3}, {121, 11}};
  for (const auto& test : tests_firstFactor)
  {
    Bigint a(test.first);
    cout << "Test " << testIndex++ << ": firstFactor(" << a << ") = " << test.second << endl;
    TestsResult &= displayResult(Primality::firstFactor(a) == test.second);
  }

  // Pollard
  // ex: 540 143 = 421 x 1 283
  // 421 - 1 = 420 = 2^2 x 3 x 5 x 7 : 421 is 7-smooth
  // 180 = 2^2 x 3^2 x 5
  Bigint t1("540143");
  
  cout << "Test " << testIndex++ << ": pollard p-1 of " << t1 << endl;
  TestsResult &= displayResult(Primality::Pollard_p_minus_one(t1, 2, 8) == 421);

  cout << "Test " << testIndex++ << ": Rho de Pollard Floyd of " << t1 << endl;
  TestsResult &= displayResult(Primality::Pollard_Rho_Floyd(t1, 2) == 421);

  Bigint f6 = Primality::FermatNumber(6);
  cout << "Test " << testIndex++ << ": Rho de Pollard Floyd of " << f6 << endl;
  TestsResult &= displayResult(Primality::Pollard_Rho_Floyd(f6, 2) == 274177);
  
  cout << "Test " << testIndex++ << ": Rho de Pollard Brent of " << t1 << endl;
  TestsResult &= displayResult(Primality::Pollard_Rho_Brent(t1, 2) == 421);

  // 1 403 = 61 x 23
  // 61 - 1 = 60 = 2^2 x 3 x 5
  // 23 - 1 = 22 = 2 x 11
  cout << "Test " << testIndex++ << ": pollard p-1 of 1 403" << endl;
  TestsResult &= displayResult(Primality::Pollard_p_minus_one(1403, 2, 6) == 61);
 
  // 2 993 = 41 x 73
  // 41 - 1 = 40 = 2^3 x 5
  // 73 - 1 = 72 = 2^3 x 3^2
  cout << "Test " << testIndex++ << ": pollard p-1 of 2 993" << endl;
  TestsResult &= displayResult(Primality::Pollard_p_minus_one(2993, 2, 6) == 41);

  // 10 001 = 73 x 137
  // 73 - 1 = 72 = 2^3 x 3^2
  // 137 - 1 = 136 = 2^2 x 3 x 13
  cout << "Test " << testIndex++ << ": pollard p-1 of 10 001" << endl;
  TestsResult &= displayResult(Primality::Pollard_p_minus_one(10001, 2, 6) == 73);

  // 172 189 = 421 × 409
  // 409 - 1 = 2^3 x 3 x 17 so 2^(17!) = 1 mod 172 189
  cout << "Test " << testIndex++ << ": pollard p-1 of 172 189" << endl;
  TestsResult &= displayResult(Primality::Pollard_p_minus_one(172189, 2, 17) == 421);
  
  // 6 994 241 = 3 361 x 2 081
  // 3 361 - 1 = 3 360 = 2^5 x 3 x 5 x 7 is 7-smooth
  // 2 081 - 1 = 2 080 = 2^5 x 5 x 13 so 2^(13!) = 1 mod 6 994 241
  cout << "Test " << testIndex++ << ": pollard p-1 of 6 994 241" << endl;
  TestsResult &= displayResult(Primality::Pollard_p_minus_one(6994241, 2, 14) == 3361);
  
  //cout << "Test " << testIndex++ << ": pollard p-1 of 2^67-1" << endl;
  //TestsResult &= displayResult(Primality::Pollard_p_minus_one((two^67) - 1, 3, 3000) == 193707721);
  
  vector<string> tests_Fermat {"3", "5", "17", "257", "65 537", "4 294 967 297", "18 446 744 073 709 551 617",
    "340 282 366 920 938 463 463 374 607 431 768 211 457"};
  for (int i = 0; i < 8; ++i)
  {
    cout << "Test " << testIndex++ << ": fermat numbers " << i << endl;
    TestsResult &= displayResult(Primality::FermatNumber(i) == tests_Fermat[i]);
  }
  
  vector<string> tests_Fibonacci {"0", "1", "1", "2", "3", "5", "8", "13", "21", "34", "55",
    "89", "144", "233", "377", "610", "987", "1597", "2584", "4181", "6765", "10946"};
  
  cout << "Test " << testIndex++ << ": 14 Fibonacci numbers (via Lucas sequence)" << endl;
  TestsResult &= displayResult(Primality::LucasSequence(14, 1, -1).first == tests_Fibonacci[15]);
  cout << endl;

  for (int i = 0; i < 22; ++i)
  {
    cout << "Test " << testIndex++ << ": Fibonacci number (" << i << ")=" << tests_Fibonacci[i] << endl;
    TestsResult &= displayResult(Primality::FibonacciNumber(i) == tests_Fibonacci[i]);
    cout << endl;
  }
  
  vector<tuple<Bigint, Bigint, int>> tests_JacobiSymbol {
    {      13,       19, -1},
    {      14,       51,  1},
    {     219,      383,  1},
    {     610,      987, -1},
    {    1001,     9907, -1},
    {    6278,     9975, -1},
    {20406967, 76545467, -1}};
  for (const auto& test : tests_JacobiSymbol)
  {
    cout << "Test " << testIndex++ << ": Jacobi (" << get<0>(test) << ", " << get<1>(test) << ") = " << get<2>(test) << endl;
    TestsResult &= displayResult(Primality::JacobiSymbol(get<0>(test), get<1>(test)) == get<2>(test));
  }

  cout << "Test " << testIndex++ << ": Solovay Strassen of 2 251 * 11 251" << endl;
  TestsResult &= displayResult(Primality::SolovayStrassen(2251 * 11251, 7) == PrimalityStatus::NOT_PRIME);
  
  cout << "Test " << testIndex++ << ": Miller Rabin of 561" << endl;
  TestsResult &= displayResult(Primality::MillerRabbin(561, 5) == PrimalityStatus::NOT_PRIME);
  
  cout << "Test " << testIndex++ << ": Miller Rabin of 1 373 653" << endl;
  TestsResult &= displayResult(Primality::MillerRabbin(1373653, 5) == PrimalityStatus::NOT_PRIME);

  cout << "Test " << testIndex++ << ": Miller Rabin Deterministic of 1 373 653" << endl;
  TestsResult &= displayResult(Primality::MillerRabbinDeterministic(1373653) == PrimalityStatus::NOT_PRIME);

  vector<pair<string, int>> tests_Williams_p_plus_one {
    {"112729", 5},
    {"112729", 9},
    {"190581011", 5},
    {"1908510861011", 5}};
  for (const auto& test : tests_Williams_p_plus_one)
  {
    Bigint number(test.first);
    cout << "Test " << testIndex++ << ": Williams p+1 applied to " << number << endl;
    TestsResult &= displayResult(Primality::Williams_p_plus_one(test.second, number) == PrimalityStatus::NOT_PRIME);
  }
  
  //cout << "test Primality: Williams p+1" << endl;
  //Bigint w2("315951348188966255352482641444979927");
  //primality::Williams_p_plus_one(0, w2);
  
  cout << "Test " << testIndex++ << ": Goldbach for n between 4 and 98" << endl;
  Primality::goldbach(100, true);
  cout << endl;
  
  cout << "Test " << testIndex++ << ": PSW for n between 3 and 297" << endl;
  TestsResult &= displayResult(Primality::PSWconjecture(3, 300));
  
  return displayResult(TestsResult, "s Primality");
}

bool Tests::TestCrypto()
{
  cout << "------------\nTests Crypto\n------------\n" << endl;
  bool TestsResult = true;
  
  cout << "Test " << testIndex++ << ": RSA encode/decode 12345" << endl;
  bool rsa = Crypto::RSA(157, 233, 5, 12345);
  TestsResult &= displayResult(rsa);
  
  cout << "Test " << testIndex++ << ": RSA encode/decode 1234567890" << endl;
  Bigint message("1234567890");
  rsa = Crypto::RSA(35393, 45161, 3, message);
  TestsResult &= displayResult(rsa);
  
  cout << "Test " << testIndex++ << ": RSA encode/decode 1234567890" << endl;
  Bigint rsa_p("876789674523635393");
  Bigint rsa_q("1454376543098765197");
  rsa = Crypto::RSA(rsa_p, rsa_q, 65537, message);
  TestsResult &= displayResult(rsa);
  
  return displayResult(TestsResult, "s Crypto");
}

bool Tests::TestPerfs()
{
  bool TestsResult = true;
  
  auto t_start = std::chrono::high_resolution_clock::now();
  
  for (int i = 0; i < 3; ++i)
    TestsResult &= Test(TestsType::ALL);
  
  //for (Bigint n = 3; n < 1000; ++n)
  //  Primality::inversesZ_nZ(n);

  auto t_end = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<float> fs1 = t_end - t_start;
  std::chrono::milliseconds d1 = std::chrono::duration_cast<std::chrono::milliseconds>(fs1);
  cout << "Duration: " << d1.count() << endl;

  return displayResult(TestsResult, "s Perfs");
}

bool Tests::TestSyracuse()
{
  cout << "--------------\nTests Syracuse\n--------------\n" << endl;
  bool TestsResult = true;
  Syracuse s;
  
  int delay = 21;
  Agent min = two^delay;
  Agent agent(16);
  string way = "";
  int nbWays = 0;
  s.findClassRecord(agent, 4, delay, min, way, nbWays);
  cout << "Test " << testIndex++ << " Find class record (delay=" << delay << ") : " << min << "\n" << endl;
  TestsResult &= displayResult(min.flightDelay() == delay);
  
  vector<pair<int, int>> tests_delay {
    {       1,   0},
    {       2,   1},
    {     329,  50},
    {     167,  67},
    {11200681, 688}};
  for (const auto& test : tests_delay)
  {
    cout << "Test " << testIndex++ << ": flight delay(" << test.first << ")=" << test.second << endl;
    Agent t(test.first);
    TestsResult &= displayResult(t.flightDelay() == test.second);
  }
  
  //int t[LIMB_MAX];
  //s.flightLengthTable(b, LIMB_MAX, t);
  //s.test(b, 100000, 100, t);
  
  Agent start(0);
  for (int i = 1; i < 14; ++i)
  {
    cout << "Test " << testIndex++ << ": delay class " << i << endl;
    Syracuse::delayClass(i, start);
  }
  
  cout << "Test " << testIndex++ << ": build delay class" << endl;
  Syracuse::buildDelayClass(31);

  cout << "Test " << testIndex++ << ": verifyTh1(12)" << endl;
  TestsResult &= displayResult(Syracuse::verifyTh1(12));

  return displayResult(TestsResult, "s Syracuse");
}

bool Tests::Test(TestsType type)
{
  testIndex = 1;
  bool result = true;
  
  switch (type)
  {
    case TestsType::PARSING:
      result = TestParsing();
      break;
    case TestsType::DISPLAY:
      result = TestDisplay();
      break;
    case TestsType::DIGITS:
      result = TestDigits();
      break;
    case TestsType::OPERATORS:
      result = TestOperators();
      break;
    case TestsType::INEQUALITIES:
      result = TestInequalities();
      break;
    case TestsType::STRESS:
      result = TestStress();
      break;
    case TestsType::PRIMALITY:
      result = TestPrimality();
      break;
    case TestsType::CRYPTO:
      result = TestCrypto();
      break;
    case TestsType::PERFS:
      result = TestPerfs();
      break;
    case TestsType::SYRACUSE:
      result = TestSyracuse();
      break;
    case TestsType::ALL:
      result = TestParsing()
            && TestDisplay()
            && TestDigits()
            && TestOperators()
            && TestInequalities()
            && TestPrimality()
            && TestCrypto()
            && TestSyracuse();
      displayResult(result, "s");
      break;
  }
  return result;
}

bool Tests::displayResult(bool isOK, const string& s)
{
  if (isOK)
  {
    cout << "Test" << s << " OK\n" << endl;
  }
  else
  {
    cout << "Test" << s << " fail\n" << endl;
    assert(false);
  }
  
  return isOK;
}

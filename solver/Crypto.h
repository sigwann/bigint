//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#pragma once

#include "Bigint.h"

class Crypto
{
public:
  //RSA
  static bool RSA(const Bigint& p, const Bigint& q, const Bigint& e, const Bigint& m);
};

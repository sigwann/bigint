//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "EllipticCurve.h"
#include "Primality.h"
#include <math.h> // for sqrt and log

// N=F5 s=6 u=31 v=24 a=3 959 199 962 b=3 489 971 618 x=29791 y=489335 z=13824
void SuyamaParametrisation(const Bigint& S, const Bigint& N, Bigint& A, ECpoint& P)
{
  int i;
  Bigint temp1, temp2, temp3, temp4, temp5, temp6, temp7 = S, temp8, Q (4);
  
  temp8 = S * S - 5;
  cout << "u=" << temp8 << " ";

  temp7 *= 4;
  cout << "v=" << temp7 << " ";
  
  temp6 = temp7.extendedEuclide(N)[0];
  temp1 = temp6;
  // 1/v
  temp6 = temp8.extendedEuclide(N)[0];
  temp2 = temp6;
  // 1/u
    
  temp6 = Q.extendedEuclide(N)[0];
  Q = temp6;
  if (temp8 <= temp7) temp3 = temp7 - temp8;
  else
  {
    temp7 += N;
    temp3 = temp7 - temp8;
    temp7 -= N;
  }
  temp4 = temp3 * temp3;
  temp4 %= N;
  temp5 = temp3 * temp4;
  temp5 %= N;
  temp6 = temp8;
  temp6 *= 3;
  temp6 += temp7;
  temp3 = temp5 * temp6;
  temp3 %= N;
  temp4 = temp3 * temp8;
  temp4 %= N;
  temp3 = temp4 * temp2;
  temp3 %= N;
  temp4 = temp3 * temp2;
  temp4 %= N;
  temp3 = temp4 * temp1;
  temp3 %= N;
  A = temp3 * Q;
  A += N;
  A--;
  A--;
  A %= N;
  cout << "a=" << A << " ";
  //a=((v - u) * (v - u) * (v - u) * (3 u + v) / (4 * u * u * u * v)) - 2; cout << "a : " << a << endl;
    
  temp3 = temp1 * temp1;
  temp3 %= N;
  temp4 = temp3 * temp1;
  temp4 %= N;
  temp5 = temp4 * temp8;
  temp5 %= N;
  cout << "b=" << temp5 << " ";
  // b = u / (v * v * v)
    
  temp3 = temp8 * temp8;
  temp3 %= N;
  P.x = temp3 * temp8;
  P.x %= N;
  cout << "x=" << P.x << " ";
  // x = u * u * u
    
  temp8 = S;
  temp3 = temp8 * temp8;
  temp3--;
  temp3 %= N;
  temp4 = temp8 * temp8;
  for (i = 0; i < 25; ++i)
    temp4--;
  temp4 %= N;
  temp5 = temp3 * temp4;
  temp5 %= N;
  temp3 = temp8 * temp8;
  temp3 %= N;
  temp4 = temp3 * temp3;
  for (i = 0; i < 25; ++i)
    temp4--;
  temp4 %= N;
  temp6 = temp4 * temp5;
  temp6 %= N;
  cout << "y=" << temp6 << " ";
  // y = (s * s - 1) * (s * s - 25) * (s * s * s * s - 25);
    
  temp3 = temp7 * temp7;
  temp3 %= N;
  P.z = temp3 * temp7;
  P.z %= N;
  cout << "z=" << P.z << " ";
  // z = v * v * v
}

void EllipticCurve::ECM(const Bigint& N, string chaine, const int B1, const int B2)
{
  Bigint T, A, U(1), Q(4), S(chaine), W6, W7; //S=352
  ECpoint P;
  int b, i, j, k = 0, l, m, n, p, s, B = 1000;
    
  // pre calculation of prime numbers. 2 < B < B1
  vector<int> tp = Primality::firstPrimesTable(B);
  
  W6 = Q.extendedEuclide(N)[0];
  Q = W6;
  b = 0;
  while (b == 0)
  {
    // Phase 1
    n = 2;
    i = 3;
    cout << S << endl;
    //SuyamaParametrisation(S, N, A, P);
    // calculation of (A + 2) / 4
    T = A + U + 1;
    A = T * Q;
    A %=N;
    while (i < B1)
    {
      m = 1;
      l = int(log(B1) / log(i));
      for (k = 0; k < l; ++k)
        m *= i;
      P.montgomeryLadder(N, A, m);
      if (n < B - 1)
      {
        n++;
        i = tp[n];
      }
      else
      {
        p = 0;
        while (p == 0)
        {
          i += 2;
          j = 3;
          s = (int)sqrt(i);
          while ((i % j != 0) && j <= s)
            j += 2;
          if (j > s)
            p = 1;
        }
      }
    }
    W7 = Primality::GCD(P.z, N);
    if (W7 > U)
    {
      b = 1;
      cout << W7 << "   S : " << S;
    }
    else
    {
      cout << "phase 2" << endl;
    }
        
    // Phase 2
    if (b == 0)
    {
      p = 0;
      while (p == 0)
      {
        i += 2;
        j = 3;
        s = (int)sqrt(i);
        while ((i % j != 0) && j <= s)
          j += 2;
        if (j > s)
          p = 1;
      }
      while (i < B2)
      {
        P.montgomeryLadder(N, A, i);
        p = 0;
        while (p == 0)
        {
          i += 2;
          j = 3;
          s = (int)sqrt(i);
          while ((i % j != 0) && j <= s)
            j += 2;
          if (j > s)
            p = 1;
        }
      }
      W7 = Primality::GCD(P.z, N);
      if (W7 > U)
      {
        b = 1;
        cout << W7 << "   S : " << S;
      }
      else
      {
        S++;
        cout << "new curve" << endl;
      }
    }
  }
}

// by≤z=x^3+ax≤z+xz≤
void EllipticCurve::VerifSuyama(const Bigint& N)
{
  string nb1 = "3 959 199 962";
  string nb2 = "3 489 971 618";
  string nb3 = "29 791";
  string nb4 = "489 335";
  string nb5 = "13 824";
    
  Bigint LM, RM, W7, W9, WA (nb1), WB (nb2), WX (nb3), WY (nb4), WZ (nb5);
    
  W7 = WY * WY * WB;
  LM = W7 * W7;
  LM %=N;
    
  RM = WX * WX * WX;
  RM %= N;
  W9 = WZ * WZ * WX;
  RM += W9;
  RM %= N;
  W7 = WX * WX * WZ * WA;
  W7 %= N;
  RM += W7;
  RM %= N;
    
  cout << "Left member : " << LM << endl;
  cout << "Right member : " << RM << endl;
}

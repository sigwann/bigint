//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "Bigint.h"

class ECpoint
{
public:
  Bigint x;
  Bigint z;
    
  // Add 2 points on elliptic curve
  void montgomeryAdd(const ECpoint&, const ECpoint&, const Bigint&);
  // Multiply by 2 a point on elliptic curve
  void montgomeryDoubling(const Bigint&, const Bigint&);
  // kP with P point on an elliptic curve
  void montgomeryLadder(const Bigint&, const Bigint&, const int);
};

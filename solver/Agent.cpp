//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "Agent.h"

const int Agent::LENGTH_ALERT = 9999999;

Agent& Agent::operator = (const Agent & copy)
{
  mLimbs = copy.mLimbs;
  mSign = copy.mSign;
  mNeedNormalization = copy.mNeedNormalization;
  return *this;
}

Agent& Agent::operator = (const Bigint& copy)
{
  mLimbs = copy.mLimbs;
  mSign = copy.mSign;
  mNeedNormalization = copy.mNeedNormalization;
  return *this;
}

void Agent::spot()
{
  randBI(100, 900);
  cout << "My initial place: " << toStringFormated() << endl;
}

int Agent::flightDelay() const
{
  int length = 0;
  Agent B(*this);
  
  while (B != 1)
  {
    B.applyOriginalModus();
    length++;
    if (length > LENGTH_ALERT)
      toast();
  }
  
  return(length);
}

long Agent::flightDescription()
{
  long length = 0;
  Agent B(*this);
  
  while (B != 1)
  {
    if (!B.isEven())
    {
      B.goOdd();
      cout << B << endl;
      length++;
    }
    B.dividePerTwo();
    cout << B << endl;
    length++;
  }
  
  cout << "\nFlight length: " << length << endl;
  
  return length;
}

void Agent::goEven()
{
  for (int i = (int)size() - 1; i >= 0; --i)
  {
    if (mLimbs[i] % 2 == 0)
      mLimbs[i] >>= 1;
    else
    {
      mLimbs[i] = (mLimbs[i] - 1) / 2;
      mLimbs[i - 1] += LIMB_MAX;
    }
  }
  
  if (mLimbs.back() == 0)
    normalize();
}

void Agent::goOdd()
{
  for (auto& limb : mLimbs)
  {
    limb *= 3;
    if (!mNeedNormalization && limb >= LIMB_MAX)
      mNeedNormalization = true;
  }
  
  mLimbs[0]++;
  if (!mNeedNormalization && mLimbs[0] >= LIMB_MAX)
    mNeedNormalization = true;
  
  if (mNeedNormalization)
    normalize();
}

void Agent::applyOriginalModus()
{
  if (isEven())
    goEven();
  else
    goOdd();
}

void Agent::toast() const
{
  cout << "!!! Alert !!!\nnumber: " << *this << "\nnb digits: " << this->nbDigits() << endl;
  assert(false);
}

vector<Agent> Agent::predecessors() const
{
  vector<Agent> result {(*this) * 2};
  
  if (!isEven())
    return result;
  
  Agent temp = *this - 1;
  
  Agent q;
  Agent r;
  temp.division(3, q, r);
  
  if (r == 0 && !q.isEven())
    result.push_back(q);

  return result;
}

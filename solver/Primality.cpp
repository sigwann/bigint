//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "Primality.h"
#include <math.h> // for sqrt

const vector<int> Primality::primesTable = {  2,   3,   5,   7,  11,  13,  17,  19,  23,  29,
                                             31,  37,  41,  43,  47,  53,  59,  61,  67,  71,
                                             73,  79,  83,  89,  97, 101, 103, 107, 109, 113,
                                            127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
                                            179, 181, 191, 193, 197, 199, 211, 223, 227, 229};

PrimalityStatus Primality::MillerRabbin(const Bigint& n, int k)
{
  for (int i = 0; i < k; ++i)
  {
    auto a = n.randomValue();
    cout << "Witness " << a << endl;
    if (MillerRabbinWitness(a, n))
      return PrimalityStatus::NOT_PRIME;
  }
  
  return PrimalityStatus::LIKELY_PRIME;
}

PrimalityStatus Primality::MillerRabbinDeterministic(const Bigint& n)
{
  // According to Pomerance, Selfridge, Wagstaff and Jaeschke
  // If we do Riemann hypothesis assumption
  // Following witnesses could be sufficient
  vector<Bigint> witnesses;
  if (n < Bigint("1 373 653"))
    witnesses = {2, 3};
  else if (n < Bigint("9 080 191"))
    witnesses = {31, 73};
  else if (n < Bigint("4 759 123 141"))
    witnesses = {2, 7, 61};
  else if (n < Bigint("2 152 302 898 747"))
    witnesses = {2, 3, 5, 7, 11};
  else if (n < Bigint("3 474 749 660 383"))
    witnesses = {2, 3, 5, 7, 11, 13};
  else if (n < Bigint("341 550 071 728 321"))
    witnesses = {2, 3, 5, 7, 11, 13, 17};
  else if (n < Bigint("3 825 123 056 546 413 051"))
    witnesses = {2, 3, 5, 7, 11, 13, 17, 19, 23};
  else if (n < Bigint("318 665 857 834 031 151 167 461"))
    witnesses = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};
  else return MillerRabbin(n, 20); // could be replaced by Eric Bach bounds

  for (auto witness : witnesses)
  {
    cout << "Witness " << witness << endl;
    if (MillerRabbinWitness(witness, n))
      return PrimalityStatus::NOT_PRIME;
  }
  
  return PrimalityStatus::LIKELY_PRIME;
}

bool Primality::MillerRabbinWitness(const Bigint& a, const Bigint& n)
{
  Bigint temp1 = n - 1;
  Bigint temp2 = temp1;
  int nbTwoPower = temp1.nbTwoPower();
  for (int i = 0; i < nbTwoPower; ++i)
    temp2.dividePerTwo();
 
  auto x = a.exponentiationBySquaringModular(temp2, n);
  
  if (x == 1 || x == n - 1)
    return false;
  
  while (nbTwoPower > 1)
  {
    x = x.exponentiationBySquaringModular(2, n);
    if (x == n - 1)
      return false;
    else
      nbTwoPower--;
  }
  
  return true;
}

template <class T>
T Primality::firstFactor(T number)
{
  if (number <= 1)
    return 0; // zero and one are not prime
  if (number <= 3)
    return 1; // two and three are prime
  if (number % 2 == 0)
    return 2;
  if (number % 3 == 0)
    return 3;

  // Check only 6k±1 numbers
  // Since 2 divides 6k, 6k+2, 6k+4
  // And   3 divides 6k+3
  
  for (T i = 5; i * i <= number; i += 4)
  {
    if (number % i == 0)
      return i;
    
    i += 2;
    
    if (number % i == 0)
      return i;
  }

  return 1;
}

bool Primality::isPrime(const Bigint& number)
{
  if (number.abs() == 0 || number.abs() == 1)
    return false;
  if (number.abs() == 2 || number.abs() == 3 || number.abs() == 5)
    return true;
  
  if (number.isEven()
   || number.unit() == 5
   || number.sumOfDigits() % 3 == 0)
    return false;
  
  return Primality::firstFactor(number) == 1;
}

vector<int> Primality::firstPrimesTable(int n)
{
  int i, j, s;
  vector<int> p;
  
  if (n < 3)
    n = 3;
  p.push_back(2);
  
  i = 3;
  while (p.size() < n)
  {
    s = (int)sqrt(i);
    j = 3;
    while (i%j != 0 && j <= s)
      j += 2;
    if (j > s)
      p.push_back(i);
    i += 2;
  }
  
  return p;
}

vector<int> primesSmallerThan(int n)
{
  int i, j, s;
  vector<int> p;
  
  if (n < 3)
    n = 3;
  p.push_back(2);
  
  i = 3;
  while (p[p.size() - 1] < n)
  {
    s = (int)sqrt(i);
    j = 3;
    while ((i % j != 0) && j <= s)
      j += 2;
    if (j > s)
      p.push_back(i);
    i += 2;
  }
  
  return p;
}

Bigint Primality::GCD(const Bigint& a, const Bigint& b)
{
  Bigint temp1 = b.abs();
  Bigint temp2(0);
  Bigint result = a.abs();
  
  while (temp1 != 0)
  {
    temp2  = temp1;
    temp1  = result % temp2;
    result = temp2;
  }
  
  return result;
}

Bigint Primality::Pollard_p_minus_one(const Bigint& A, const Bigint& base, int m)
{
  Bigint temp = base;
  Bigint result;
  
  for (int i = 2; i <= m; ++i)
  {
    temp = temp.exponentiationBySquaringModular(i, A);
    result = GCD(temp - 1, A);
    if (result > 1 && result < A)
      break;
  }
  
  /*for (int i = 2; i <= m; ++i)
    temp = temp.exponentiationBySquaringModular(i, A);
  temp--;
  result = GCD(temp, A);*/
  
  if (result > 1 && result < A)
    cout << "Pollard p-1 success: " << result << endl;
  else
  {
    cout << "Pollard p-1 fail" << endl;
    result = 0;
  }
  
  return result;
}

// X^2 + 1
Bigint Pol1(const Bigint& b, const Bigint& m) { return b.exponentiationBySquaringModular(2, m) + 1; }

// X^1024 + 1
Bigint Pol2(const Bigint& b, const Bigint& m) { return b.exponentiationBySquaringModular(1024, m) + 1; }

Bigint Primality::Pollard_Rho_Floyd(const Bigint& A, const Bigint& B)
{
  Bigint (*Pol) (const Bigint&, const Bigint&);
  Pol = Pol1;
  
  Bigint temp1 = B;
  auto temp2 = Pol(temp1, A);
  auto result = GCD(temp2 - temp1, A);
  
  int index = 0;
  while (result == 1)
  {
    temp1 = Pol(temp1, A);
    temp2 = Pol(Pol(temp2, A), A);

    result = GCD(temp2 - temp1, A);
    index++;
    if (index % 1000 == 0)
      cout << "index=" << index << endl;
  }
  
  if (result == A)
    cout << "Pollard's Rho Floyd fail" << endl;
  else
    cout << "Factor: " << result << endl;
  
  return result;
}

Bigint Primality::Pollard_Rho_Brent(const Bigint& A, const Bigint& B)
{
  Bigint (*Pol) (const Bigint&, const Bigint&);
  Pol = Pol1;

  Bigint temp1;
  Bigint temp2 = B;
  auto temp3 = Pol(temp2, A);
  Bigint temp4(1);
  auto result = GCD(temp3 - temp2, A);
  
  while (result == 1)
  {
    temp1 = temp4;
    temp1 *= 2;
    temp2 = temp3;
    while (temp1 >= temp4 && result == 1)
    {
      temp3 = Pol(temp3, A);
      if (result != 3)
        result = GCD(temp3 - temp2, A);
      temp4++;
    }
  }
  if (result == A)
    cout << "Pollard's Rho Brent fail" << endl;
  else
    cout << "Factor: " << result << endl;
  
  return result;
}

int Primality::JacobiSymbol(const Bigint& a, const Bigint& b)
{
  Bigint u = a;
  Bigint v = b;
  
  if (u == 0 || v.isEven())
    return 0;

  int j = 1;

  if (u.mSign == -1)
  {
    u.mSign = 1;
    if ((v % 4) == 3)
      j = -1;
  }
  
  while (u > 1)
  {
    if (!u.isEven())
    {
      if ((u % 4) == 3 && (v % 4) == 3)
        j = -j;
      Bigint r = v % u;
      v = u;
      u = r;
      /* Quadratic reciprocity: JacobiSymbol(a,b)=-JacobiSymbol(b,a) if a=3 or b=3 (mod 4) */
    }
    else
    {
      int k = 0;
      while (u.isEven())
      {
        /* Process factors of 2: JacobiSymbol(2,b) = -1 if b = 3,5 (mod 8) */
        u.dividePerTwo();
        k++;
      }
      if (((k&1) == 1) && ((v % 8) == 3 || (v % 8) == 5))
        j = -j;
    }
  }
  
  return (u == 1 ? j : 0);
}

PrimalityStatus Primality::SolovayStrassen(const Bigint& n, int k)
{
  for (int i = 0; i < k; ++i)
  {
    Bigint a;
    do
    {
      a = n.randomValue();
    }
    while (GCD(a, n) != 1);
    
    cout << "Witness " << a << endl;
    
    Bigint e = n - 1;
    e.dividePerTwo();
    if (a.exponentiationBySquaringModular(e, n) != JacobiSymbol(a, n))
      return PrimalityStatus::NOT_PRIME;
  }

  return PrimalityStatus::LIKELY_PRIME;
}

PrimalityStatus Primality::Williams_p_plus_one(int B, const Bigint& N)
{
  int nb = B == 0 ? 1000 : 1;
  PrimalityStatus status = PrimalityStatus::PRIME;
  
  for (int i = 0; i < nb; ++i)
  {
    Bigint A = B == 0 ? i == 1 ? 5 : rand() % 10000 : B;
  
    for (int m = 2; m < 100; ++m)
    {
      Bigint x(A);
      Bigint y = A * A - 2;
      y %= N;
      
      int val = 1;
      while (val <= m)
        val <<= 1;
      
      if (val > 1)
        val >>= 1;

      if (val > 1)
        val >>= 1;

      while (val >= 1)
      {
        if (val&m)
        {
          x = (x * y - A) % N;
          y = (y * y - 2) % N;
        }
        else
        {
          y = (x * y - A) % N;
          x = (x * x - 2) % N;
        }
        val >>= 1;
      }
    
      auto p = GCD(x - 2, N);
      if (p > 1 && p < N)
      {
        status = PrimalityStatus::NOT_PRIME;
        cout << "factor: " << p << endl;
        break;
      }
      else
        A = x;
    }
  }
  if (status == PrimalityStatus::PRIME)
    cout << "no factor found" << endl;
  
  return status;
}

Bigint Primality::FermatNumber(int n)
{
  Bigint result = (two^(two^n)) + 1;
  
  cout << "2^(2^" << n << ")+1 = " << result << endl;
  
  return result;
}

Bigint Primality::MersennePrimeNumber(int n)
{
  vector<Bigint> p {2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607, 1279, 2203, 2281, 3217, 4253, 4423, 9689, 9941};
  
  if (n < 1 || n >= p.size())
    return 0;
  
  return (two^(p[n - 1])) - 1;
}

pair<Bigint, Bigint> Primality::LucasSequence(int n, int p, int q)
{
  Bigint Un_minus_2(0);
  Bigint Un_minus_1(1);
  Bigint Un;
  
  Bigint Vn_minus_2(2);
  Bigint Vn_minus_1(p);
  Bigint Vn;
  
  for (int i = 0; i < n; ++i)
  {
    Un = Un_minus_1 * p - Un_minus_2 * q;
    cout << "U_" << i + 2 << " = " << Un << "    ";
    Un_minus_2 = Un_minus_1;
    Un_minus_1 = Un;
    
    Vn = Vn_minus_1 * p - Vn_minus_2 * q;
    cout << "V_" << i + 2 << " = " << Vn << endl;
    Vn_minus_2 = Vn_minus_1;
    Vn_minus_1 = Vn;
  }
  
  return make_pair(Un, Vn);
}

Bigint Primality::FibonacciNumber(int n)
{
  if (n < 1)
    return 0;
  if (n == 1)
    return 1;
  
  Bigint result(0);
  Bigint temp(1);
  
  for (int i = 1; i <= n; ++i)
  {
    Bigint temp2 = result;
    result += temp;
    temp = temp2;
  }
  
  //cout << "Fibonacci number (" << n << ")=" << result << endl;
  
  return result;
}

void Primality::goldbach(Bigint n, bool allSolutions)
{
  int cacheSize = 1000; // tested till 100 000
  if (n < cacheSize)
    cacheSize = n.to_int();
  vector<PrimalityStatus> primeCache;
  for (int i = 0; i < cacheSize; ++i)
    primeCache.push_back(firstFactor(i) == 1 ? PrimalityStatus::PRIME : PrimalityStatus::NOT_PRIME);

  if (n % 2 == 1)
    n--;
  
  if (n < 4)
    n = 4;
  
  int nbNotFound = 0;
  
  for (Bigint i = 4; i < n; i += 2)
  {
    Bigint nb = 0;
    Bigint maxj = i;
    maxj.dividePerTwo();
    for (Bigint j = 2; j <= maxj; ++j)
    {
      if (j < cacheSize)
      {
        if (primeCache[j.to_int()] == PrimalityStatus::NOT_PRIME)
          continue;
      }
      else
      {
        if (firstFactor(j) != 1)
          continue;
      }
      
      Bigint complement = i - j;
      
      if (complement < cacheSize)
      {
        if (primeCache[complement.to_int()] == PrimalityStatus::NOT_PRIME)
          continue;
      }
      else
      {
        if (firstFactor(complement) != 1)
          continue;
      }

      if (nb == 0)
        cout << i << " = ";
      cout << j << " + " << complement << "; ";

      nb++;
      if (!allSolutions)
       break;
    }
    if (nb == 0)
    {
      nbNotFound++;
      cout << "Not found: " << i << endl;
    }
    else
      cout << "(" << nb << " solutions)" << endl;
  }
  if (nbNotFound > 0)
  {
    cout << "# not found: " << nbNotFound << endl;
    assert(false);
  }
}

bool Primality::PSWconjecture(int start, int end, bool log)
{
  bool conjecture = true;
  
  for (int p = start; p < end; p += 6)
  {
    for (int i = 0; i < 2; ++i)
    {
      bool psw = true;
      if (log)
        cout << "test PSW conjecture of " << p << endl;
      
      auto b1 = two.exponentiationBySquaringModular(p - 1, p);
      if (log)
        cout << "2^(" << p << "-1) = " << b1 << " [" << p << "]" << endl;
      if (b1 != 1)
      {
        psw = false;
        if (log)
          cout << p << " is not prime" << endl;
      }
      
      if (psw)
      {
        auto b2 = Primality::FibonacciNumber(p + 1) % p;
        if (log)
          cout << "Fibonacci number F_(" << p << "+1) = " << b2 << " [" << p << "]" << endl;
        if (b2 != 0)
        {
          psw = false;
          if (log)
            cout << p << " is not prime according to conjecture" << endl;
        }
      }
      
      bool testPrimality = Primality::isPrime(p);
      
      if (log)
      {
        if (testPrimality)
          cout << "After check, " << p << " is prime" << endl;
        else
          cout << "After check, " << p << " is not prime" << endl;
      }
      
      if (testPrimality != psw)
      {
        conjecture = false;
        cout << "Conjecture is false" << endl;
        assert(false);
      }
      
      if (log)
        cout << endl;
      if (i == 0)
        p += 4;
    }
  }
  
  return conjecture;
}

void Primality::inversesZ_nZ(const Bigint& n)
{
  int nb = 1;
  for (Bigint m = 2; m < n; ++m)
  {
    auto ee = m.extendedEuclide(n);
    if (ee.empty())
      continue;
    nb++;
    auto i = ee[0];
    if (i < m)
      continue;
    cout << m << "^{-1} [" << n << "]=" << i << endl;
    assert( (m * i) % n == 1);
  }
  cout << "phi(" << n << ")=" << nb << "\n" << endl;
}

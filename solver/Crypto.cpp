//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

//   512 bits: 155 digits (key not sure anymore)
// 1 024 bits: 309 digits
// 2 048 bits: 617 digits (adviced length)

#include "Crypto.h"
#include "Primality.h"

bool Crypto::RSA(const Bigint& p, const Bigint& q, const Bigint& e, const Bigint& m)
{
   // Bob chooses p and q
  Bigint n = p * q;
  // Bob calculates phi(n)
  Bigint phi = (p - 1) * (q - 1);
  // Bob chooses e prime with phi(n)
  assert(Primality::GCD(e, phi) == 1);
  auto d = e.extendedEuclide(phi);
  // Bob publishes public key (n, e)
  // d is Bob's private key
  
  // Message encoding by Alice
  auto c = m.exponentiationBySquaringModular(e, n);
  // Message decoding by Bob
  auto mm = c.exponentiationBySquaringModular(d[0], n);
  
  return m == mm;
}

//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "Syracuse.h"

FILE * sortie;

void Syracuse::flightLengthTable(Agent& agent, int p, int *t)
{
  for (int k = 0; k < p; ++k)
  {
    t[k] = agent.flightDelay();
    cout << "t[" << k << "]=" << t[k] << endl;
    agent++;
  }
}

void Syracuse::test(Agent& agent, long p, int nbp, int *t)
{
  long i;
  int l, length, maxlength;
  Agent B;
  Agent C;
  Agent D;
    
  if ((agent.mLimbs[0] & 1) == 0)
    agent++;
    
  C = agent;
  maxlength = 1;
  for (l = 0; l < nbp; ++l)
  {
    cout << "new block: " << agent << endl;
        
    for (i = 0; i < p; ++i)
    {
      B = agent;
      length = 1;
      while (B.size() > 1)
      {
        if (B.isEven())
        {
          B.goEven();
          length++;
        }
        else
        {
          B.goOdd();
          B.goEven();
          length += 2;
        }
      }
            
      length += t[B.mLimbs[0] - 1] - 1;
            
      if (length > maxlength)
      {
        D = C;
        D *= 2;
        if (D < agent)
        {
          maxlength += 1;
          C = D;
          cout << C << " ---length---> " << maxlength << endl;
          if (length > maxlength)
          {
            maxlength = length;
            C = agent;
            cout << C << " ---length---> " << maxlength << endl;
          };
        }
        else
        {
          maxlength = length;
          C = agent;
          cout << C << " ---length---> " << maxlength << endl;
        }
      }
            
      agent += 2;
            
      /* Sieve !!!! */
      if ((agent.mLimbs[0] % 8) == 5)
        agent += 2;
    }
  }
}

void Syracuse::randomTrials(int n)
{
  int length;
  Agent agent(0);
    
  for (int i = 0; i < n; ++i)
  {
    cout << "Trial #" << i << endl;
    agent.spot();
    length = agent.flightDelay();
    cout << "Flight delay: " << length << endl;
  }
}

void Syracuse::randomTable()
{
  int i, j, l, n, m;
  int maxt = 15000;
  Agent R, tab[15000];
  FILE *file;
  char temp[600];
  char *indice;
    
  sortie = fopen("resultats.txt", "w");
    
  // Load of alea.txt
  file = fopen("alea.txt","r");
  for (i = 0; i < maxt; ++i)
  {
    fgets(temp, 600, file);
    indice = strchr(temp,':');
    indice += 2;
    indice[strlen(indice) - 1] = '\0';
    tab[i].parseString(indice);
  }
  fclose(file);
    
  srand ((unsigned int)(time (NULL)));
  i = 0;
  n = 0;
  m = 0;
  while (i < 1000000) //(i<2147483645)
  {
    R.randBI(0, 1000);
    cout << R << endl;
    l = R.flightDelay();
    //cout << l << endl;
    if (l < maxt)
    {
      if (tab[l] == 0)
      {
        tab[l] = R;
        n++;
      }
      else
      {
        if (R < tab[l])
        {
          tab[l] = R;
          m++;
        }
      }
    }
    else printf("Alert overflow: %d\n", l);
    i++;
    //if (i == 2000000001) i = 1;
    //if (i % 10000 == 0) cout << i << endl;
  }
  for (j = 0; j < maxt; ++j)
  {
    fprintf(sortie, "%d : ", j);
    fprintf(sortie, "%s", tab[j].toStringFormated().c_str());
    fprintf(sortie, "\n");
  }
  printf("New: %d\n", n);
  printf("Modified: %d\n", m);
}

void Syracuse::delayClass(int c, const Agent& start)
{
  cout << "Class " << c << ": ";
  Agent B = start;
  int nb = 0;
  
  Agent bgmax(1000);
  if (c > 6)
  {
    Agent root(2);
    bgmax = root^c;
  }
  
  while (B < bgmax)
  {
    B++;
    if (B.flightDelay() == c)
    {
      cout << B << ", ";
      nb++;
    }
    if (nb % 10 == 0)
      cout << std::flush;
  }
  cout << "(" << nb << ")\n" << endl;
}

void Syracuse::buildDelayClass(int delayMax)
{
  int limit = 30; // Do not display all class element if number of elements is behind this limit
  
  vector<Agent> agents {8};
  
  if (delayMax < 4)
    delayMax = 4;
  
  cout << "class1: 2 (1)\n\nclass2: 4 (1)\n\nclass3: 8 (1)\n" << endl;

  for (int delay = 4; delay <= delayMax; ++delay)
  {
    vector<Agent> newAgents;
    cout << "delay" << delay << ": ";
    for (const auto& agent : agents)
    {
      auto pred = agent.predecessors();
      newAgents.insert(newAgents.end(), pred.begin(), pred.end());
    }
    agents = newAgents;
    
    // Print class elements
    if (agents.size() < limit)
    {
      sort(agents.begin(), agents.end());
      size_t size = agents.size();
      for (size_t j = 0; j < size; ++j)
      {
        cout << agents[j];
        if (j < size - 1)
          cout << ",";
        cout << " ";
      }
    }
    cout << "(" << agents.size() << ")\n" << endl;
  }
}

void Syracuse::findClassRecord(Agent agent, int index, int delay, Agent& record, string way, int& nbWays)
{
  if (index == delay)
  {
    nbWays++;
    if (agent < record)
    {
      record = agent;
      cout << "new min " << record << " (rank:"  << nbWays << ")\n" << way << endl;
    }
  }
  else if (index < delay)
  {
    index++;
    auto pred = agent.predecessors();
    if (pred.size() == 2)
      findClassRecord(pred[1], index, delay, record, way + "L", nbWays);
    findClassRecord(pred[0], index, delay, record, way + "R", nbWays);
  }
}

void Syracuse::findClassRecords()
{
  for (int delay = 5; delay < 100000; ++delay)
  {
    Agent base(2);
    Agent min = base^delay;
    Agent agent(16);
    string way = "";
    int nbWays = 0;
    findClassRecord(agent, 4, delay, min, way, nbWays);
    cout << "Find class record (delay=" << delay << ") : " << min << ", nb elements: " << nbWays << endl;
  }
}

bool Syracuse::verifyTh1(int n)
{
  cout << "2^" << n << "-1 -> ... -> 3^" << n << "-1" << endl;
  
  int length = 0;
  
  Agent start(2);
  start = (start^n) - 1;
  
  Agent end(3);
  end = (end^n) - 1;
  
  while (start != end)
  {
    length++;
    cout << start << endl;
    start.applyOriginalModus();
    
    if (length > Agent::LENGTH_ALERT)
    {
      start.toast();
      return false;
    }
  }
  cout << end << "\nlength = " << length << endl;

  return true;
}

bool Syracuse::powerOfThreeMinusOneFligt(int n)
{
  cout << "3^" << n << "-1 flight description" << endl;
  
  Agent start(3);
  start = (start^n) - 1;
  cout << start << endl;
  start.flightDescription();
  
  return true;
}

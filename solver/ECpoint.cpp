//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "ECpoint.h"

void ECpoint::montgomeryAdd(const ECpoint& Q, const ECpoint& P0, const Bigint& N)
{
  Bigint temp1, temp2, temp3;
    
  // X_P+Q = Z_P-Q((X_P - Z_P)(X_Q + Z_Q) + (X_P + Z_P)(X_Q - Z_Q))≤
  // X_P+Q = 4 * Z_P - Q * (X_P * X_Q - Z_P * Z_Q)^2
    
  temp1 = x * Q.x;
  temp1 %= N;
  temp2 = z * Q.z;
  temp2 %= N;
  temp3 = (temp2 > temp1) ? temp2 - temp1 : temp1 - temp2;
  temp1 = temp3 * temp3;
  temp1 %= N;
  temp1 *= 4;
  x = P0.z * temp1;
  x %= N;
  // X_P+Q is put in X_P
    
  // Z_P+Q = X_P-Q((X_P - Z_P)(X_Q + Z_Q) - (X_P + Z_P)(X_Q - Z_Q))≤
  // Z_P+Q = 4 * X_P-Q * (X_P * Z_Q - Z_P * X_Q)^2
  temp1 = x * Q.z;
  temp1 %= N;
  temp2 = z * Q.x;
  temp2 %= N;
  temp3 = (temp2 > temp1) ? temp2 - temp1 : temp1 - temp2;
  temp1 = temp3 * temp3;
  temp1 %= N;
  temp1 *= 4;
  z = P0.x * temp1;
  z %= N;
  // Z_P+Q is put in Z_P
}

void ECpoint::montgomeryDoubling(const Bigint& A, const Bigint& N)
{
  Bigint temp1, temp2, temp3, temp4;
    
  // X_2P = (X_P + Z_P)^2(X_P - Z_P)^2
  temp1 = x + z;
  temp2 = temp1 * temp1;
  temp2 %= N;
  temp1 = (x >= z) ? x - z : z - x;
  temp3 = temp1 * temp1;
  temp3 %= N;
  temp1 = x * z;
  temp1 %= N;
  x = temp2 * temp3;
  x %= N;
    
  // Z_2P = 4 * X_P * Z_P((X_P - Z_P)^2 + A * (4 * X_P * Z_P))
  temp1 *= 4;
  temp4 = A * temp1;
  temp4 %= N;
  temp2 = temp3 + temp4;
  z = temp1 * temp2;
  z %= N;
}

void ECpoint::montgomeryLadder(const Bigint& N, const Bigint& A, const int q)
{
  int qb = q, d = 1;
  ECpoint temp1, temp2;
    
  while (d < q)
    d *= 2;
    
  // Q
  temp1 = *this;
  // P
  temp2 = *this;
  // P = 2P
  temp2.montgomeryDoubling(A, N);
  if (d > qb)
    d >>= 1;
  qb -= d;
  while (qb > 0)
  {
    if (d < qb)
    {
      temp1.montgomeryAdd(temp2, *this, N);
      temp2.montgomeryDoubling(A, N);
      qb -= d;
      if (d == 0)
        qb = 0;
    }
    else
    {
      temp1.montgomeryDoubling(A, N);
      temp2.montgomeryAdd(temp1, *this, N);
    }
    d >>= 1;
  }
  *this = temp1;
}

//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.

// Lychrel numbers
//
// A Lychrel number is a natural number that cannot form a palindrome
// through the iterative process of repeatedly reversing its digits
// and adding the resulting numbers


// tested till 700 000

// Nb palindromes
// 0-10^1 :     10
// 0-10^2 :     19
// 0-10^3 :    109
// 0-10^4 :    199
// 0-10^5 :   1099
// 0-10^6 :   1999
// 0-10^7 :  10999
// 0-10^8 :  19999
// 0-10^9 : 109999

// TODO
// determine all numbers giving 64 steps. they are like x5029y

#include "Bigint.h"

class Lychrel
{
  public:

  static vector<tuple<Bigint, Bigint, int>> lychrelTable;

  static void explore(int start, int end, bool log, bool saveAbort);
  static Bigint palindrome(Bigint num, int& nbSteps, bool& abort);
  static void findFirstNumberGivenNbStep(const Bigint& start);
};

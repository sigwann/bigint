//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include "Bigint.h"
#include <regex>
#include <algorithm>
#include <math.h>

const int    Bigint::BASE      = 10;
const int    Bigint::MAXDIGITS = 4;
const int    Bigint::LIMB_MAX     = pow(BASE, MAXDIGITS); // LIMB_MAX must be a power of BASE.
const int    Bigint::KARATSUBA = 50;
const string Bigint::SEPARATOR = " ";

static tuple<Bigint, Bigint, Bigint> cache_preMontgomery;
static Bigint cache_modulo;

Bigint::Bigint()
{
  mSign  = 1;
  mLimbs = vector<int>();
  mNeedNormalization = false;
}

Bigint::Bigint(const Bigint& copy)
{
  mSign  = copy.mSign;
  mLimbs = copy.mLimbs;
  mNeedNormalization = copy.mNeedNormalization;
}

Bigint::Bigint(int t)
{
  if (t < 0)
  {
    mSign = -1;
    t *= -1;
  }
  else
    mSign = 1;
  
  while (t > 0 || size() == 0)
  {
    mLimbs.push_back(t % LIMB_MAX);
    t = (int)((t - mLimbs.back()) / LIMB_MAX);
  }
}

void Bigint::parseString(string s)
{
  mSign = 1;
  string compactS = "";

  reset();
  
  // Remove spaces between figures
  size_t length = s.length();
  for (size_t i = 0; i < length; ++i)
    if (s[i] != ' ')
      compactS += s[i];

  // Regular expression
  string input = compactS;
  regex rgx("(\\+|-)?[[:digit:]]+");
  smatch match;
  regex_search(input, match, rgx);
  compactS = match[0];
  
  //assert(!compactS.empty());
  //TODO throw an exception
  if (compactS.empty())
    compactS = "0";
  
  // Find out number's sign
  if (compactS[0] == '-')
  {
    mSign = -1;
    compactS = compactS.substr(1, compactS.length() - 1);
  }
  else if (compactS[0] == '+')
  {
    mSign = 1;
    compactS = compactS.substr(1, compactS.length() - 1);
  }

  length = compactS.length();
  size_t nbLimbs = size_t(length / MAXDIGITS);
  
  for (size_t i = 0; i < nbLimbs; ++i)
  {
    string temp = compactS.substr(length - (i + 1) * MAXDIGITS, MAXDIGITS);
    mLimbs.push_back(stoi(temp));
  }
  
  if (length == nbLimbs * MAXDIGITS)
    return;
  
  string temp = compactS.substr(0, length - nbLimbs * MAXDIGITS);
  mLimbs.push_back(stoi(temp));
}

Bigint& Bigint::operator = (const Bigint& copy)
{
  mLimbs = copy.mLimbs;
  mSign = copy.mSign;
  mNeedNormalization = copy.mNeedNormalization;
  
  return *this;
}

Bigint& Bigint::operator = (const int x)
{
  reset();
  int temp = x;
  
  if (temp < 0)
  {
    mSign = -1;
    temp *= -1;
  }
  else
    mSign = 1;

  while (temp > 0 || size() == 0)
  {
    mLimbs.push_back(temp % LIMB_MAX);
    temp = (int)(temp / LIMB_MAX);
  }
  
  return *this;
}

Bigint& Bigint::operator = (string s)
{
  parseString(s);
  
  return *this;
}

Bigint Bigint::operator + (const Bigint& t) const
{
  Bigint result;
  Bigint temp1(*this);
  temp1.mSign = 1;
  Bigint temp2(t);
  temp2.mSign = 1;
  
  if (mSign * t.mSign == 1) // (|a|+|b|)
  {
    result = temp1.addition(temp2);
    result.mSign = mSign;
  }
  else
  {
    result = temp1 - temp2;
    if (result != 0)
      result.mSign *= mSign;
  }
  
  return result;
}

Bigint Bigint::addition(const Bigint& t) const
{
  Bigint result;
  Bigint temp1(*this);
  Bigint temp2(t);
  int carry = 0;
  
  auto m = max(size(), t.size());
  result.mLimbs.insert(result.mLimbs.begin(), m, 0);
  if (m > temp1.size())
    temp1.mLimbs.insert(temp1.mLimbs.end(), m - temp1.size(), 0);
  if (m > temp2.size())
    temp2.mLimbs.insert(temp2.mLimbs.end(), m - temp2.size(), 0);

  for (size_t i = 0; i < m; ++i)
  {
    int sum = carry + temp1.mLimbs[i] + temp2.mLimbs[i];
    
    if (sum < LIMB_MAX)
    {
      carry = 0;
    }
    else
    {
      sum %= LIMB_MAX;
      carry = 1;
    }
    
    result.mLimbs[i] = sum;
  }
  if (carry > 0)
    result.mLimbs.push_back(carry);
  
  return result;
}

Bigint Bigint::operator * (const Bigint& t) const
{
  if (size() < KARATSUBA || t.size() < KARATSUBA)
    return standardMultiplication(t);
  
  return karatsubaMultiplication(t);
}

Bigint Bigint::operator * (const int x) const
{
  Bigint result;
  
  if (x == 0)
    return 0;
  
  int temp = x;
  
  if (temp < 0)
  {
    result.mSign = -mSign;
    temp *= -1;
  }
  else
    result.mSign = mSign;
  
  result.mLimbs = mLimbs;
  for (auto& limb : result.mLimbs)
  {
    limb *= temp;
    if (!result.mNeedNormalization && limb > LIMB_MAX)
      result.mNeedNormalization = true;
  }
  
  if (result.mNeedNormalization)
    result.normalize();
  
  return result;
}

Bigint Bigint::standardMultiplication(const Bigint& t) const
{
  Bigint result;
  
  size_t resultSize = size() + t.size();
  result.mSign = mSign * t.mSign;
  result.mLimbs.insert(result.mLimbs.begin(), resultSize, 0);
  
  for (size_t i = 0; i < size(); ++i)
    for (size_t j = 0; j < t.size(); ++j)
    {
      result.mLimbs[i + j] += mLimbs[i] * t.mLimbs[j];
      if (result.mLimbs[i + j] > LIMB_MAX && i + j < resultSize)
      {
        int a = result.mLimbs[i + j] % LIMB_MAX;
        result.mLimbs[i + j + 1] += (result.mLimbs[i + j] - a) / LIMB_MAX;
        result.mLimbs[i + j] = a;
      }
    }
  
  result.normalize();
  
  return result;
}

Bigint Bigint::high(size_t t) const
{
  if (t > size())
    return Bigint(0);
  
  Bigint result;
  
  result.mSign = mSign;
  copy(mLimbs.begin() + t, mLimbs.end(), back_inserter(result.mLimbs));

  return result;
}

Bigint Bigint::low(size_t t) const
{
  if (t >= size())
    return *this;
  
  Bigint result;

  result.mSign = mSign;
  copy(mLimbs.begin(), mLimbs.begin() + t, back_inserter(result.mLimbs));
  
  return result;
}

Bigint Bigint::karatsubaMultiplication(const Bigint& t) const
{
  size_t maxSize = max(size(), t.size());
  size_t middle = (size_t)((maxSize + 1) / 2);
  
  Bigint a1 = high(middle);
  Bigint a2 = low(middle);
  Bigint b1 = t.high(middle);
  Bigint b2 = t.low(middle);

  Bigint a = a1 * b1;
  Bigint d = a2 * b2;
  Bigint e = (a1 + a2) * (b1 + b2) - a - d;
  
  a.mLimbs.insert(a.mLimbs.begin(), middle * 2, 0);
  e.mLimbs.insert(e.mLimbs.begin(), middle, 0);

  a += e + d;
  
  return a;
}

Bigint Bigint::operator - (const Bigint& t) const
{
  Bigint result;
  Bigint temp1(*this);
  temp1.mSign = 1;
  Bigint temp2(t);
  temp2.mSign = 1;
  
  if (mSign * t.mSign == -1) // (|a|+|b|)
  {
    result = temp1 + temp2;
    result.mSign = mSign;
  }
  else
  {
    if (temp1 == temp2)
    {
      result = 0;
    }
    else if (temp1 > temp2)
    {
      result = temp1.subtraction(temp2);
      result.mSign = mSign;
    }
    else
    {
      result = temp2.subtraction(temp1);
      result.mSign = -mSign;
    }
  }
  
  return result;
}

Bigint Bigint::subtraction(const Bigint& b) const
{
  Bigint result;
  
  result.mLimbs = mLimbs;
  for (size_t i = 0; i < b.size(); ++i)
    result.mLimbs[i] -= b.mLimbs[i];
  for (size_t i = 0; i < size() - 1; ++i)
  {
    if (result.mLimbs[i] < 0)
    {
      result.mLimbs[i] += LIMB_MAX;
      result.mLimbs[i + 1]--;
    }
  }
  
  result.normalize();
  
  return result;
}

Bigint& Bigint::operator -= (const Bigint& t)
{
  *this = *this - t;
  
  return *this;
}

Bigint& Bigint::operator += (const Bigint& t)
{
  *this = *this + t;
  
  return *this;
}

Bigint& Bigint::operator *= (const Bigint& t)
{
  Bigint result;
  
  size_t resultSize = size() + t.size();
  result.mSign = mSign * t.mSign;
  result.mLimbs.insert(result.mLimbs.begin(), resultSize, 0);
  
  for (size_t i = 0; i < size(); ++i)
    for (size_t j = 0; j < t.size(); ++j)
    {
      result.mLimbs[i + j] += mLimbs[i] * t.mLimbs[j];
      if (result.mLimbs[i + j] > LIMB_MAX && i + j < resultSize)
      {
        int a = result.mLimbs[i + j] % LIMB_MAX;
        result.mLimbs[i + j + 1] += (result.mLimbs[i + j] - a) / LIMB_MAX;
        result.mLimbs[i + j] = a;
      }
    }

  result.normalize();

  *this = result;

  return *this;
}

Bigint& Bigint::operator *= (int t)
{
  if (t == 0)
  {
    *this = 0;
    return *this;
  }
  
  if (t < 0)
  {
    t *= -1;
    mSign *= -1;
  }
  
  for (auto& limb : mLimbs)
  {
    limb *= t;
    if (!mNeedNormalization && limb > LIMB_MAX)
      mNeedNormalization = true;
  }
  
  if (mNeedNormalization)
    normalize();
  
  return *this;
}

bool Bigint::operator >(int B) const
{
  if (B == 0)
  {
    if (mLimbs.size() == 1 && mLimbs[0] == 0)
      return false;
    return mSign == 1;
  }
  if (mSign == 1 && B < 0)
    return true;
  if (mSign == -1 && B > 0)
    return false;

  return *this > Bigint(B);
}

bool Bigint::operator >(const Bigint& B) const
{
  if (mSign == 1 && B.mSign == -1)
    return true;
  if (mSign == -1 && B.mSign == 1)
    return false;
  if (mSign == -1 && B.mSign == -1)
    return !(B.abs() <= (*this).abs());
  if (size() > B.size())
    return true;
  if (size() < B.size())
    return false;
  
  size_t i = size();
  while (i > 0 && mLimbs[i - 1] == B.mLimbs[i - 1])
    i--;
  
  return (i == 0) ? false : mLimbs[i - 1] > B.mLimbs[i - 1];
}

bool Bigint::operator <(const Bigint& B) const
{
  return !(*this >= B);
}

bool Bigint::operator >=(int B) const
{
  if (B == 0)
  {
    if (mLimbs.size() == 1 && mLimbs[0] == 0)
      return true;
    return mSign == 1;
  }
  if (mSign == 1 && B < 0)
    return true;
  if (mSign == -1 && B > 0)
    return false;
  
  return *this >= Bigint(B);
}

bool Bigint::operator >=(const Bigint& B) const
{
  if (mSign == 1 && B.mSign == -1)
    return true;
  if (mSign == -1 && B.mSign == 1)
    return false;
  if (mSign == -1 && B.mSign == -1)
    return !(B.abs() < (*this).abs());
  if (size() > B.size())
    return true;
  if (size() < B.size())
    return false;
  
  size_t i = size();
  while (i > 0 && mLimbs[i - 1] == B.mLimbs[i - 1])
  {
    i--;
    if (i == 0)
      break;
  }
  return (i == 0) ? true : mLimbs[i - 1] >= B.mLimbs[i - 1];
}

bool Bigint::operator <=(const Bigint& B) const
{
  return !(*this > B);
}

bool Bigint::operator ==(const Bigint& B) const
{
  return mSign == B.mSign && mLimbs == B.mLimbs;
}

bool Bigint::operator ==(int B) const
{
  if (size() < 2)
    return mSign * mLimbs[0] == B;
  
  return *this == Bigint(B);
}

bool Bigint::operator !=(const Bigint& B) const
{
  return !(*this == B);
}

bool Bigint::operator !=(int B) const
{
  if (size() < 2)
    return mSign * mLimbs[0] != B;
  
  return !(*this == Bigint(B));
}

Bigint Bigint::operator %(const Bigint& b) const
{
  if (b == 0)
    return *this;
  
  Bigint temp(0);
  Bigint result = *this;
  
  while (result.size() > b.size())
  {
    temp.reset();
    temp.mSign = result.mSign * b.mSign;
    size_t tempSize = result.size() - b.size();
    temp.mLimbs.insert(temp.mLimbs.begin(), tempSize, 0);
    temp.mLimbs[tempSize - 1] = int((result.mLimbs[result.size() - 1] * LIMB_MAX
                                + result.mLimbs[result.size() - 2]) / (1 + b.mLimbs[b.size() - 1]));
    temp.normalize();
    
    result -= (temp * b);
  }
  
  if ((b > 0 && (result >= b || result < 0))
   || (b < 0 && (result <= b || result > 0)))
  {
    if ((b > 0 && result < 0 && b + result > 0)
     || (b < 0 && result > 0 && b + result < 0))
    {
      result += b;
    }
    else
    {
      temp.reset();
      temp.mSign = result.mSign * b.mSign;
      temp.mLimbs.push_back(int(result.mLimbs[result.size() - 1] / (1 + b.mLimbs[b.size() - 1])));
      //cout << temp << endl;
      //if (temp * t > result) cout << "ohoh" << endl; else cout << "ihih" << endl;
      // TODO fix bug
      //cout << "1" << endl << temp << endl;
      //cout << "2" << endl << n << endl;
        
      result -= temp * b;
      while ((b > 0 && (result >= b || result < 0)) || (b < 0 && (result <= b || result > 0)))
        result -= b * temp.mSign;
    }
  }
  
  return result;
}

Bigint& Bigint::operator %=(const Bigint& t)
{
  if (t == 0)
    return *this;
  
  Bigint temp(0);
    
  while (size() > t.size())
  {
    temp.reset();
    size_t tempSize = size() - t.size();
    temp.mLimbs.insert(temp.mLimbs.begin(), tempSize, 0);
    temp.mLimbs[tempSize - 1] = int((mLimbs[size() - 1] * LIMB_MAX + mLimbs[size() - 2]) / (1 + t.mLimbs[t.size() - 1]));
    temp.normalize();
    (*this) -= temp*t;
  }
  
  if (*this >= t)
  {
    temp.reset();
    temp.mLimbs.push_back(int(mLimbs[size() - 1] / (1 + t.mLimbs[t.size() - 1])));
    (*this) -= temp * t;
    while (*this >= t)
      (*this) -= t;
  }
  
  return *this;
}

Bigint& Bigint::operator - ()
{
  mSign *= -1;
  return *this;
}

Bigint& Bigint::operator ++ ()
{
  if (mSign == 1)
  {
    mLimbs[0]++;
    if (!mNeedNormalization && mLimbs[0] >= LIMB_MAX)
      mNeedNormalization = true;
  }
  else
  {
    mLimbs[0]--;
    
    if (mLimbs[0] < 0)
    {
      if (size() == 1)
      {
        mLimbs[0] *= -1;
        mSign *= -1;
      }
      else
      {
        mNeedNormalization = true;
        for (size_t i = 0; i < size() - 1; ++i)
        {
          if (mLimbs[i] < 0)
          {
            mLimbs[i] += LIMB_MAX;
            mLimbs[i + 1]--;
          }
        }
      }
    }
  }
  
  if (mNeedNormalization)
    normalize();
  else if (mLimbs[0] == 0 && mLimbs.size() == 1)
    mSign = 1;

  return *this;
}

Bigint Bigint::operator ++ (int)
{
  operator++();
  return *this;
}

Bigint& Bigint::operator -- ()
{
  if (mSign == 1)
  {
    mLimbs[0]--;
    
    if (mLimbs[0] < 0)
    {
      if (size() == 1)
      {
        mLimbs[0] *= -1;
        mSign *= -1;
      }
      else
      {
        mNeedNormalization = true;
        for (size_t i = 0; i < size() - 1; ++i)
        {
          if (mLimbs[i] < 0)
          {
            mLimbs[i] += LIMB_MAX;
            mLimbs[i + 1]--;
          }
        }
      }
    }
  }
  else
  {
    mLimbs[0]++;
    if (!mNeedNormalization && mLimbs[0] >= LIMB_MAX)
      mNeedNormalization = true;
  }
  
  if (mNeedNormalization)
    normalize();
  
  return *this;
}

Bigint Bigint::operator -- (int)
{
  operator--();
  return *this;
}

Bigint Bigint::MontgomeryReduction(const Bigint& R, const Bigint& R_inverse, const Bigint& Modulus, const Bigint& Modulus_opposite_inverse)
{
  // This method can be optimized
  // See algorithm 14.32 of Handbook of Applied Cryptography
  Bigint U = (*this * Modulus_opposite_inverse) % R;
  Bigint result = *this + (U * Modulus);
  result = result.exactDivision(R);
  if (result > Modulus)
    result -= Modulus;
  
  return result;
}

tuple<Bigint, Bigint, Bigint> preMontgomery(const Bigint& m)
{
  if (m == cache_modulo)
    return cache_preMontgomery;
  
  assert(!m.isEven());
  
  Bigint r(1);
  while (r < m)
    r *= 2;

  auto ee = r.extendedEuclide(m);
  //assert(r * (ee[0]) + m * (ee[1]) == ee[2]);

  cache_modulo = m;
  cache_preMontgomery = make_tuple(r, ee[0], ee[1]);
  
  return cache_preMontgomery;
}

Bigint Bigint::mulMod(const Bigint& b, const Bigint& M, const int n) const
{
  int i, cu, q;
  Bigint result(0);
    
  Bigint temp = *this;
  int bu = b.unit();
    
  for (int k = 0; k < n; ++k)
  {
    cu = result.unit();
    if (temp.isEven())
      i = 0;
    else
    {
      i = 1;
      temp--;
    }
    temp *= 5;
    temp.shiftRight();
    q = (cu + bu * i) % 2;
    if (i == 1)
      result += b;
    for (int j = 0; j < q; ++j)
      result += M;
    result *= 5;
    result.shiftRight();
  }
    
  return result;
}

Bigint Bigint::operator ^ (int B) const
{
  assert(B >= 0);

  Bigint temp(*this);
  Bigint result(1);
    
  while (B > 0)
  {
    if ((B&1) != 0)
      result *= temp;
    temp *= temp;
    B >>= 1;
  }
  
  return result;
}

Bigint Bigint::operator ^ (const Bigint& B) const
{
  assert(B >= 0);
  
  Bigint p = B;
  Bigint temp(*this);
  Bigint result(1);
    
  while (p > 0)
  {
    if ((p.unit()&1) != 0)
    {
      result *= temp;
      p--;
    }
    temp *= temp;
    p.dividePerTwo();
  }
  return result;
}

Bigint Bigint::exponentiationBySquaringModular(int exponent, const Bigint& m) const
{
  int e = exponent;
  Bigint temp(*this);
  Bigint result(1);
  
  if (m == 1)
    while (e > 0)
    {
      if ((e&1) != 0)
        result *= temp;
      temp *= temp;
      e >>= 1;
    }
  else if (e < 100)
  {
    temp = (*this) % m;
    while (e > 0)
    {
      if ((e&1) != 0)
      {
        result = (result * temp) % m;
      }
      temp = (temp * temp) % m;
      e >>= 1;
    }
  }
  else
  {
    auto v = preMontgomery(m); // r , r^-1, m^-1
    Bigint r = get<0>(v);
    //assert(r > 0);
    Bigint r_inv = get<1>(v);
    //assert(r_inv > 0);
    Bigint m_inv_opp = -get<2>(v);
    //assert(m_inv_opp < 0);
    //assert((r * r_inv) + (m * m_inv_opp) == 1);
    //assert((m * m_inv_opp) % r == -1);

    Bigint phi = ((*this) * r) % m;
    
    temp = (*this);// % m;
    while (e > 0)
    {
      if ((e&1) != 0)
      {
        result = (result * phi).MontgomeryReduction(r, r_inv, m, m_inv_opp);
      }
      phi = (phi * phi).MontgomeryReduction(r, r_inv, m, m_inv_opp);
      e >>= 1;
      //cout << result << "," << temp1;
      //cout << " , p : " << p << endl;
    }
  }
  //cout << toStringFormated() << " ^ " << B << " mod " << M;
  //cout << " = " << result << "\n" << endl;
  return result;
}

Bigint Bigint::exponentiationBySquaringModular(const Bigint& exponent, const Bigint& M) const
{
  Bigint e = exponent;
  Bigint temp(*this);
  Bigint result(1);
  
  if (M == 1)
  {
    while (e > 0)
    {
      if ((e.unit()&1) != 0)
      {
        result *= temp;
      }
      temp *= temp;
      e.dividePerTwo();
    }
  }
  else
  {
    temp = (*this) % M;
    while (e > 0)
    {
      if ((e.unit()&1) != 0)
      {
        result = (result * temp) % M;
        e--;
      }
      temp = (temp * temp) % M;
      e.dividePerTwo();
      //cout << result << "," << temp1;
      //cout << " , p : " << p << endl;
    }
  }
  //cout << toStringFormated() << " ^ " << B << " mod " << M;
  //cout << " = " << result << "\n" << endl;
  return result;
}

void Bigint::normalize()
{
  size_t i;
  int carry = 0;

  for (i = 0; i < size(); ++i)
  {
    int x = mLimbs[i] + carry;
    
    if (x >= LIMB_MAX)
    {
      int r = x % LIMB_MAX;
      carry = (int)((x - r) / LIMB_MAX);
      mLimbs[i] = r;
    }
    else
    {
      mLimbs[i] = x;
      carry = 0;
    }
  }
  
  if (carry != 0)
    while (carry != 0)
    {
      if (carry >= LIMB_MAX)
      {
        int r = carry % LIMB_MAX;
        mLimbs.push_back(r);
        carry = (int)((carry - r) / LIMB_MAX);
      }
      else
      {
        mLimbs.push_back(carry);
        carry = 0;
      }
      i++;
    }
  else
    while (i > 1 && mLimbs[i - 1] == 0)
    {
      mLimbs.pop_back();
      i--;
    }
  
  if (size() == 1 && mLimbs[0] == 0)
    mSign = 1;
  
  mNeedNormalization = false;
}

string Bigint::toString() const
{
  string result;
  string temp;
  
  for (size_t index = size(); index > 0; --index)
  {
    temp = to_string(mLimbs[index - 1]);
    
    // Add leading zeros on the left to handle cases like "54" -> "0054"
    if (index < size())
    {
      int nbZero = MAXDIGITS - (int)temp.length();
      for (int i = 0; i < nbZero; ++i)
        temp = "0" + temp;
    }
    
    result.append(temp);
  }
  
  if (mSign == -1)
    result = "-" + result;
  
  return result;
}

string Bigint::toStringFormated() const
{
  string result = toString();
  size_t length = result.length();
  size_t length_top = length % 3;
  size_t nb = length == 0 ? 0 : (size_t)((length - 1) / 3);
  if (length_top == 0)
  {
    length_top = 3;
    //if (nb > 0)
      //nb--;
  }
  for (size_t i = 0; i < nb; ++i)
    result.insert(4 * i + length_top, SEPARATOR);
  
  return result;
}

int Bigint::nbDigits() const
{
  int nb = std::max(0, int(mLimbs.size() - 1)) * MAXDIGITS;
  string s = to_string(mLimbs.back());
  nb += s.length();
  
  return nb;
}

int Bigint::sumOfDigits() const
{
  int sum = 0;
  
  for (auto limb : mLimbs)
  {
    string s = to_string(limb);
    for (size_t i = 0; i < s.length(); ++i)
    {
      int c = limb % BASE;
      sum += c;
      limb = (limb - c) / BASE;
    }
  }

  return sum;
}

void Bigint::shiftRight()
{
  int u, v;
  
  if (size() > 1)
    for (size_t i = size() - 1; i > 0; --i)
    {
      u = int(mLimbs[i] / BASE);
      v = mLimbs[i] - BASE * u;
      mLimbs[i] = u;
      mLimbs[i - 1] += LIMB_MAX * v;
    }
  mLimbs[0] = int(mLimbs[0] * .1f);
  
  normalize();
}

void Bigint::shiftLeft()
{
  (*this) *= BASE;
}

int Bigint::to_int() const
{
  int result = 0;
  auto s = size();
  int m = 1;
  
  assert(LIMB_MAX < powf(INT_MAX, 1 / (float)s));
  
  for (size_t i = 0; i < s; ++i)
  {
    result += mLimbs[i] * m;
    m *= LIMB_MAX;
  }
  
  return result;
}

Bigint Bigint::abs() const
{
  Bigint result(*this);
  result.mSign = 1;
  
  return result;
}

int Bigint::nbTwoPower() const
{
  int result = 0;
  Bigint temp(*this);

  while (temp.isEven() && temp != 0)
  {
    temp.dividePerTwo();
    result++;
  }
  
  return result;
}

void Bigint::dividePerTwo()
{
  for (int i = (int)size() - 1; i >= 0; --i)
    if (mLimbs[i] % 2 == 0)
    {
      mLimbs[i] >>= 1;
    }
    else
    {
      mLimbs[i] = (mLimbs[i] - 1) / 2;
      mLimbs[i - 1] += LIMB_MAX;
    }
  
  if (mLimbs.back() == 0)
    normalize();
}

Bigint Bigint::exactDivision(const Bigint& t) const
{
  Bigint result(0);
  Bigint tmp(*this);
  
  while (tmp >= t)
  {
    tmp -= t;
    result++;
  }
  
  return result;
}

Bigint Bigint::squareRoot()
{
  int k, u, v;
  Bigint temp1(0);
  Bigint temp2(1); // successive odds
  Bigint result(0); // the square root
    
  for (size_t i = size(); i > 0; --i)
  {
    u = int(mLimbs[i - 1] / 100);
    for (k = 0; k < u; ++k)
      temp1.mLimbs[0]++;
    temp1.normalize();
    while (temp2 <= temp1)
    {
      result++;
      temp1 -= temp2;
      temp2++;
      temp2++;
    }
    temp2--;
    temp2.shiftLeft();
    temp2++;
    result.shiftLeft();
    temp1.shiftLeft();
    temp1.shiftLeft();
        
    v = mLimbs[i - 1] - 100 * u;
    for (k = 0; k < v; ++k)
      temp1.mLimbs[0]++;
    temp1.normalize();
    while (temp2 <= temp1)
    {
      result++;
      temp1 -= temp2;
      temp2++;
      temp2++;
    }
    temp2--;
    temp2.shiftLeft();
    temp2++;
    result.shiftLeft();
    temp1.shiftLeft();
    temp1.shiftLeft();
  }
    
  result.shiftRight();
  // Verification par le calcul du carre
  temp2 = result * result;
  cout << "Square root verification: " << temp2 << endl;
    
  return result;
}

void Bigint::division(const Bigint& b, Bigint& q, Bigint& r) const
{
  //assert(*this > 0);
  //assert(b > 0);

  Bigint temp;
    
  r = *this;
  q = 0;
  while (r.size() > b.size())
  {
    size_t tempSize = r.size() - b.size();
    temp.mLimbs.insert(temp.mLimbs.begin(), tempSize, 0);
    temp.mLimbs[tempSize - 1] = int((r.mLimbs[r.size() - 1] * LIMB_MAX + r.mLimbs[r.size() - 2]) / (1 + b.mLimbs[b.size() - 1]));
    temp.normalize();
    q += temp;
    r -= temp * b;
    temp.reset();
  }
  if (r >= b)
  {
    temp = 0;
    temp.mLimbs[0] = int(r.mLimbs[r.size() - 1] / (1 + b.mLimbs[b.size() - 1]));
    q += temp;
    r -= temp * b;
    while (r >= b)
    {
      r -= b;
      q.mLimbs[0]++;
    }
    q.normalize();
  }
}

// temp1 = gcd(a, b)
vector<Bigint> Bigint::extendedEuclide(const Bigint& b) const
{
    Bigint q;
    Bigint r;
    Bigint temp;
    
    Bigint temp1 = *this; // invariant1: temp1 = a * temp2 + b * temp3
    Bigint temp2(1);
    Bigint temp3(0);
    
    Bigint temp4 = b; // invariant2: temp4 = a * temp5 + b * temp6
    Bigint temp5(0);
    Bigint temp6(1);
    
    while (temp4 != 0)
    {
      temp1.division(temp4, q, r);
      
      temp  = temp1;
      temp1 = temp4;
      temp4 = temp - q * temp4;
      
      temp  = temp2;
      temp2 = temp5;
      temp5 = temp - q * temp5;
      
      temp  = temp3;
      temp3 = temp6;
      temp6 = temp - q * temp6;
      
      //cout << "invariant " << temp1 << " = " << *this << " * " << temp2 << " + " << b << " * " << temp3 << endl;
    }
    
    if (temp1 != 1)
    {
      // cout << "No inverse" << endl;
      return {};
    }
  
    //assert(temp1 == (*this) * temp2 + b * temp3);
  
    if (temp2 < 0)
      while (temp2 < 0)
      {
        temp2 += b;
        temp3 -= (*this);
        //assert(temp1 == (*this) * temp2 + b * temp3);
      }
    
  return {temp2, temp3, temp1}; // this inverse, b inverse, pgcd
}

void Bigint::randBI(int a, int b)
{
  if (size() > 0)
    reset();
  
  int t = a + rand() % b;
  
  if (t < 1)
    t = 1;
  
  for (int i = 0; i < t; ++i)
    mLimbs.push_back(rand() % LIMB_MAX);
}

Bigint Bigint::randomValue() const
{
  Bigint result;
 
  do
  {
    size_t resultSize = rand() % size();
    if (resultSize < 1)
      resultSize = 1;
    
    for (size_t j = 0; j < resultSize; ++j)
    {
      if (j == size() - 1)
      {
        int v = mLimbs[j];
        if (v > 0)
          result.mLimbs.push_back(rand() % v);
        else
          result.mLimbs.push_back(0);
      }
      else
        result.mLimbs.push_back(rand() % LIMB_MAX);
    }
  
    result.normalize();
  }
  while (result > *this);
  
  return result;
}

bool Bigint::isPalindrome()
{
  auto s = toString();
  return s == string(s.rbegin(), s.rend());
}

Bigint Bigint::reverse()
{
  auto s = toString();
  return Bigint(string(s.rbegin(), s.rend()));
}

ostream& operator<<(ostream& os, const Bigint& t)
{
  os << t.toStringFormated();
  return os;
}

//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//
//  Reference page of records: http://www.ericr.nl/wondrous/
//
// Numbers with '*' are class records
//
//  1
//  2
//  4*
//  8*
// 16*
//  5*
// 10*-20
//  3* 40---------------------------------------------------------------------80
//  6* 13                                                                    160
// 12* 26                                                                     53
// 24* 52                                                                    106
// 48* 17*                                                                    35
//     34*                                                                    70
//     11*                                                                    23
//     22*--------------------------44                                        46
//      7*                          88                                        92
//     14*                          29                                       184
//     28*--56                      58---------------------------------116    61
//      9* 112                      19                                 232   122
//     18*  37                      38                                  77   244
//     36*  74                      76---152                           154   488
//     72* 148                      25*  304                           308   976
//          49*                     50   101                           616   325
//          98*                    100   202--------------404          205   650
//         196                      33*   67              808          410  1300
//          65*                          134              269          820   433
//         130*---------------260        268              538         1640   866
//          43*               520         89              179         3280  1732
//          86*               173        178              358         1093   577
//         172*--344          346         59              119         2186  1154
//          57*  688---1376   115        118---236        238---476   4372  2308
//         114*  229   2752   230         39*  472         79   952   1457  4616
//               458    917   460         78*  157        158   317   2914  9232
//               916   1834   153*             314        316   634    971  3077
//               305*   611                    628        105*  211   1942  6154
//               610   1222                    209              422    647  2051
//               203*   407                    418---836        844   1294  4102
//               406*   814                    139  1672        281    431  1367
//               135*   271                    278   557        562    862  2734
//               270*   542                    556  1114       1124    287   911
//               540*  1084                    185*  371       2248    574  1822--3644
//                      361*                   370   742        749    191   607  7288
//                      722                    123*  247       1498    382  1214  2429
//                     1444                    246*  494        499    127  2428  4858
//                      481*                         988        998    254   809  1619
//                      962                          329*      1996    508  1618  3238
//                     1924                          658        665    169*  539  1079
//                      641*                         219*      1330         1078  2158
//                     1282---2564                              443          359   719
//                      427*  5128                              886          718  1438
//                      854   1709                              295*         239   479
//                     1708   3418                                           478   958
//                      569*  1139                                           159*  319
//                     1138*  2278                                                 638
//                      379*  4556                                                1276
//                      758*  9112                                                 425
//                     1516   3037                                                 850---1700
//                      505*  6074                                                 283*  3400
//                     1010* 12148                                                 566   1133
//                     2020   4049                                                1132   2266
//                      673*  8098                                                 377*   755
//                            2699                                                 754   1510
//                            5398                                                 251*   503
//                            1799                                                 502*  1006
//                            3598                                                 167*   335
//                            1199                                                 334*   670
//                            2398                                                 111*   223
//                             799*                                                222*   446
//                                                                                 444*   892
//                                                                                        297*

#include "Agent.h"

class Syracuse
{
public:
  // Store flight length of p first numbers (starting from agent) in the table t
  void flightLengthTable(Agent& agent, int p, int* t);
  
  void test(Agent& agent, long, int, int*);

  // Find all flights with a given delay n
  // Biggest number in class n is 2^n
  // A class can contain an odd or even number of elements
  // Open Q: number of contiguous elements in a class?
  //
  // We can also calculate number of different elements met in a class (during fligths),
  // surely way lower n * number of elements in class n
  //
  // m = number of elements in class n + 1 minus number of elements in class n
  // m is the number of odd elements in class n + 1
  static void delayClass(int n, const Agent& start);

  // Find and count all elements of the first n delay class
  // All elements of a class are listed in a vector
  // And elements of class n + 1 are found based on class n vector 
  static void buildDelayClass(int delayMax);
  
  // Build n random numbers and test corresponding flight to find out if it ends
  static void randomTrials(int n);
  
  void randomTable();
  
  // Find class record, ie the smallest number whose flight has a given delay
  // Explore the full tree of possibilities by doing reverse flights
  // nbWays: number of elements in this class
  // Rank  : rank of the class record among all class elements.
  //         A low rank means the algorithm finds the class record very quickly.
  //
  // delay:record: rank  :cardinal: way
  //     1:     2:      1:       1: R
  //     2:     4:      1:       1: RR
  //     3:     8:      1:       1: RRR
  //     4:    16:      1:       1: RRRR
  //     5:     5:      1:       2: RRRRL
  //     6:    10:      1:       2: RRRRLR
  //     7:     3:      1:       4: RRRRLRL
  //     8:     6:      1:       4: RRRRLRLR
  //     9:    12:      1:       6: RRRRLRLRR
  //    10:    24:      1:       6: RRRRLRLRRR
  //    11:    48:      1:       8: RRRRLRLRRRR
  //    12:    17:      2:      10: RRRRLRRRLRRL
  //    13:    34:      2:      14: RRRRLRRRLRRLR
  //    14:    11:      2:      18: RRRRLRRRLRRLRL
  //    15:    22:      2:      24: RRRRLRRRLRRLRLR
  //    16:     7:      2:      29: RRRRLRRRLRRLRLRL
  //    17:    14:      2:      36: RRRRLRRRLRRLRLRLR
  //    18:    28:      2:      44: RRRRLRRRLRRLRLRLRR
  //    19:     9:      2:      58: RRRRLRRRLRRLRLRLRRL
  //    20:    18:      2:      72: RRRRLRRRLRRLRLRLRRLR
  //    21:    36:      2:      91: RRRRLRRRLRRLRLRLRRLRR
  //    22:    72:      2:     113: RRRRLRRRLRRLRLRLRRLRRR
  //    23:    25:      6:     143: RRRRLRRRLRRLRLRRRLRLRRL
  //    24:    49:      3:     179: RRRRLRRRLRRLRLRLRRRRLRRL
  //    25:    98:      3:     227: RRRRLRRRLRRLRLRLRRRRLRRLR
  //    26:    33:     10:     287: RRRRLRRRLRRLRLRRRLRLRRLRRL
  //    27:    65:      3:     366: RRRRLRRRLRRLRLRLRRRRLRRLRRL
  //    28:   130:      3:     460: RRRRLRRRLRRLRLRLRRRRLRRLRRLR
  //    29:    43:      3:     578: RRRRLRRRLRRLRLRLRRRRLRRLRRLRL
  //    30:    86:      3:     732: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLR
  //    31:   172:      3:     926: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRR
  //    32:    57:      3:    1174: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRL
  //    33:   114:      3:    1489: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRLR
  //    34:    39:     68:    1879: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRL
  //    35:    78:     84:    2365: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRLR
  //    36:   153:      7:    2988: RRRRLRRRLRRLRLRLRRRRLRRLRRLRRRLRLRRL
  //    37:   305:      4:    3780: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRLRRL
  //    38:   105:    186:    4788: RRRRLRRRLRRLRLRRRLRLRRRRLRRRLRLRLRLRRL
  //    39:   203:      4:    6049: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRLRRLRL
  //    40:   406:      4:    7628: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRLRRLRLR
  //    41:   135:      4:    9635: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRLRRLRLRL
  //    42:   270:      4:   12190: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRLRRLRLRLR
  //    43:   540:      4:   15409: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRLRRLRLRLRR
  //    44:   185:    678:   19452: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRRRLRRLRLRRL
  //    45:   361:     19:   24561: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRL
  //    46:   123:   1085:   31025: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRRRLRRLRLRRLRL
  //    47:   246:   1369:   39229: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRRRLRRLRLRRLRLR
  //    48:   481:     33:   49580: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRL
  //    49:   169:   2937:   62680: RRRRLRRRLRRLRLRRRLRRRLRRRLRRRRLRRLRLRLRLRLRLRLRRL
  //    50:   329:   2772:   79255: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRRRLRRLRRRLRLRLRRL
  //    51:   641:     66:  100144: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRL
  //    52:   219:   4431:  126542: RRRRLRRRLRRLRLRRRLRLRRRRLRLRRLRLRRRLRRLRRRLRLRLRRLRL
  //    53:   427:    103:  159930: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRL
  //    54:   159:  47947:  202085: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRLRRLRLRLRLRL
  //    55:   295:   9786:  255455: RRRRLRRRLRRLRLRRRLRLRRRRLRRRLRLRLRRRLRLRRLRRRLRLRRLRLRL
  //    56:   569:    200:  322869: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRL
  //    57:  1138:    250:  408002: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLR
  //    58:   379:    315:  515542: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLRL
  //    59:   758:    402:  651407: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLRLR
  //    60:   283: 195279:  823238: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRL
  //    61:   505:    641: 1040490: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLRLRRL
  //    62:  1010:    806: 1315036: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLRLRRLR
  //    63:   377: 394258: 1661989: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRL
  //    64:   673:   1284: 2100226: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLRLRRLRRL
  //    65:   251: 629641: 2654294: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRL
  //    66:   502: 795758: 3354459: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLR
  //    67:   167:1005527: 4239037: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLRL
  //    68:   334:1270735: 5357022: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLRLR
  //    69:   111:1605954: 6769885: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLRLRL
  //    70:   222:2029728: 8555671: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLRLRLR
  //    71:   444:2565117:10812460: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLRLRLRR
  //    72:   799:   8461:13664435: RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRRRLRLRRRLRRLRLRLRLRL
  //    73:   297:4096744:17268507: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRRRLRLRLRLRLRRL
  //    74:   593:5177265:21823498: RRRRLRRRRRLRLRLRRRLRRRRLRRLRRLRRRRLRLRLRLRRRLRLRLRLRLRLRRLRLRRLRLRLRRRLRRL
  //    75:  1063:  16856:        : RRRRLRRRLRRLRLRLRRRRLRRLRRLRLRRRRRRLRLRLRLRRLRRLRRLRLRRLRLRRLRRRRLRLRRLRLRL
  //    76:   395:
  //    77:   790:
  void findClassRecord(Agent agent, int index, int delay, Agent& record, string way, int& nbWays);
  
  // Easy way to find all first class records
  // But not a smart way to find high class records, since we start from scratch for each class
  void findClassRecords();
  
  // Verify theorem 2^n-1 always reaches 3^n-1
  // Easily provable
  // And by the way, length duration to reach 3^n-1 is 2n
  // Which means that 2^n-1 flight duration is always more than twice longer than 2^n flight duration
  //
  // n =  4 :            15 -> ... -> 80
  // n =  5 :            31 -> ... -> 242
  // ...
  // n = 16 :        65 535 -> ... -> 43 046 720
  // ...
  // n = 32 : 4 294 967 295 -> ... -> 1 853 020 188 851 840
  static bool verifyTh1(int n);
  
  // Note that 2^(2N-1)-1 length + 1 = 2^(2N)-1 length
  // And so    3^(2N-1)-1 length - 1 = 3^(2N)-1 length
  //
  //        2^n-1 length 3^n-1 length 2^n+1 length
  // n=4  :          17 :          9 :         12
  // n=5  :         106 :         96 :         26
  // n=6  :         107 :         95 :         27
  // n=7  :          46 :         32 :        121
  // n=8  :          47 :         31 :        122
  // n=9  :          61 :         43 :         35
  // n=10 :          62 :         42 :         36
  // n=11 :         156 :        134 :        156
  // n=12 :         157 :        133 :        113
  // n=13 :         158 :        132 :         52
  // n=14 :         159 :        131 :         53
  // n=15 :         129 :         99 :         98
  // n=16 :         130 :         98 :         99
  // n=17 :         224 :        190 :        100
  // n=18 :         225 :        189 :        101
  // n=19 :         177 :        139 :        102
  // n=20 :         178 :        138 :         72
  bool powerOfThreeMinusOneFligt(int n);
};

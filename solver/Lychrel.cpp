//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.

#include "Lychrel.h"

// This is the table of the numbers that
// - are not lychrel numbers
// - are the smallest numbers for a given number of steps before giving a palindrome
//
// example:
// 167 is the smallest number giving a palindrome in 11 steps.
vector<tuple<Bigint, Bigint, int>> Lychrel::lychrelTable = {
  {     10,                                          Bigint("11"),  1}, //   2 digits
  {     19,                                         Bigint("121"),  2}, //   3 digits
  {     59,                                       Bigint("1 111"),  3}, //   4 digits
  {     69,                                       Bigint("4 884"),  4}, //   4 digits
  {    166,                                      Bigint("45 254"),  5}, //   5 digits
  {     79,                                      Bigint("44 044"),  6}, //   5 digits
  {    188,                                     Bigint("233 332"),  7}, //   6 digits
  {    193,                                     Bigint("233 332"),  8}, //   6 digits
  {   1397,                                  Bigint("88 555 588"),  9}, //   8 digits
  {    829,                                  Bigint("88 555 588"), 10}, //   8 digits
  {    167,                                  Bigint("88 555 588"), 11}, //   8 digits
  {   2069,                                  Bigint("52 788 725"), 12}, //   8 digits
  {   1797,                               Bigint("8 836 886 388"), 13}, //  10 digits
  {    849,                               Bigint("8 836 886 388"), 14}, //  10 digits
  {    177,                               Bigint("8 836 886 388"), 15}, //  10 digits
  {   1496,                               Bigint("5 233 333 325"), 16}, //  10 digits
  {    739,                               Bigint("5 233 333 325"), 17}, //  10 digits
  {   1798,                              Bigint("89 540 004 598"), 18}, //  11 digits
  {  10777,                          Bigint("16 967 744 776 961"), 19}, //  14 digits
  {   6999,                          Bigint("16 668 488 486 661"), 20}, //  14 digits
  {   1297,                           Bigint("8 813 200 023 188"), 21}, //  13 digits
  {    869,                           Bigint("8 813 200 023 188"), 22}, //  13 digits
  {    187,                           Bigint("8 813 200 023 188"), 23}, //  13 digits
  {     89,                           Bigint("8 813 200 023 188"), 24}, //  13 digits
  {  10797,                       Bigint("1 676 404 554 046 761"), 25}, //  16 digits
  {  10853,                       Bigint("4 455 597 447 955 544"), 26}, //  16 digits
  {  10921,                       Bigint("4 455 597 447 955 544"), 27}, //  16 digits
  {  10971,                       Bigint("8 802 202 552 022 088"), 28}, //  16 digits
  {  13297,                     Bigint("893 974 888 888 479 398"), 29}, //  18 digits
  {  10548,                      Bigint("17 858 768 886 785 871"), 30}, //  17 digits
  {  13293,                      Bigint("17 858 768 886 785 871"), 31}, //  17 digits
  {  17793,                      Bigint("44 035 358 885 353 044"), 32}, //  17 digits
  {  20889,                      Bigint("44 035 358 885 353 044"), 33}, //  17 digits
  { 700269,               Bigint("6 832 123 695 335 963 212 386"), 34}, //  22 digits
  { 106977,               Bigint("6 832 123 695 335 963 212 386"), 35}, //  22 digits
  { 108933,               Bigint("6 832 123 695 335 963 212 386"), 36}, //  22 digits
  {  80359,               Bigint("6 839 849 878 998 789 489 386"), 37}, //  22 digits
  {  13697,               Bigint("6 839 849 878 998 789 489 386"), 38}, //  22 digits
  {  10794,               Bigint("6 832 123 695 335 963 212 386"), 39}, //  22 digits
  {  15891,               Bigint("6 832 123 695 335 963 212 386"), 40}, //  22 digits
  {1009227,              Bigint("68 344 497 279 697 279 444 386"), 41}, //  23 digits
  {1007619,               Bigint("1 556 534 287 227 824 356 551"), 42}, //  22 digits
  {1009246,              Bigint("45 144 454 432 023 445 444 154"), 43}, //  23 digits
  {1008628,              Bigint("48 852 787 646 664 678 725 884"), 44}, //  23 digits
  { 600259,          Bigint("14 525 756 544 499 444 565 752 541"), 45}, //  26 digits
  { 131996,          Bigint("14 525 756 544 499 444 565 752 541"), 46}, //  26 digits
  {  70759,          Bigint("14 525 756 544 499 444 565 752 541"), 47}, //  26 digits
  {1007377,       Bigint("8 836 746 997 299 229 927 996 476 388"), 48}, //  28 digits
  {1001699,         Bigint("168 977 944 479 424 974 449 779 861"), 49}, //  27 digits
  { 600279,       Bigint("4 668 731 596 684 224 866 951 378 664"), 50}, //  28 digits
  { 141996,       Bigint("4 668 731 596 684 224 866 951 378 664"), 51}, //  28 digits
  {  70269,       Bigint("4 668 731 596 684 224 866 951 378 664"), 52}, //  28 digits
  {  10677,       Bigint("4 668 731 596 684 224 866 951 378 664"), 53}, //  28 digits
  {  10833,       Bigint("4 668 731 596 684 224 866 951 378 664"), 54}, //  28 digits
  {  10911,       Bigint("4 668 731 596 684 224 866 951 378 664"), 55}, //  28 digits
  {1009150,       Bigint("6 842 165 664 428 668 244 665 612 486"), 56}, //  28 digits
  { 600579,   Bigint("8 834 453 324 841 674 761 484 233 544 388"), 57}, //  31 digits
  { 147996,   Bigint("8 834 453 324 841 674 761 484 233 544 388"), 58}, //  31 digits
  { 178992,   Bigint("8 834 453 324 841 674 761 484 233 544 388"), 59}, //  31 digits
  { 190890,   Bigint("8 834 453 324 841 674 761 484 233 544 388"), 60}, //  31 digits
  { 600589, Bigint("682 049 569 465 550 121 055 564 965 940 286"), 63}, //  33 digits
  { 150296, Bigint("682 049 569 465 550 121 055 564 965 940 286"), 64}  //  33 digits
};

void Lychrel::explore(int start, int end, bool log, bool saveAbort)
{
  int maxSteps = 0;
  Bigint maxNum, maxPal;
  vector<Bigint> abortList;
  for (int i = start; i < end; ++i)
  {
    if (i ==  196 || i ==  295 || i ==  394 || i ==  493 || i ==  592
     || i ==  689 || i ==  691 || i ==  788 || i ==  790 || i ==  879
     || i ==  887 || i ==  978 || i ==  986 || i == 1495 || i == 1497
     || i == 1585 || i == 1587 || i == 1675 || i == 1677 || i == 1765
     || i == 1767 || i == 1855 || i == 1857 || i == 1945 || i == 1947
     || i == 1997)
      continue;
    if (i == 2494 || i == 2496 || i == 2584 || i == 2586 || i == 2674
     || i == 2676 || i == 2764 || i == 2766 || i == 2854 || i == 2856
     || i == 2944 || i == 2946 || i == 2996)
      continue;
    if (i == 3493 || i == 3495 || i == 3583 || i == 3585 || i == 3673
     || i == 3675 || i == 3763 || i == 3765 || i == 3853 || i == 3855
     || i == 3943 || i == 3945 || i == 3995)
      continue;
    if (i == 4079 || i == 4169 || i == 4259 || i == 4349 || i == 4439
     || i == 4492 || i == 4494 || i == 4529 || i == 4582 || i == 4584
     || i == 4619 || i == 4672 || i == 4674 || i == 4709 || i == 4762
     || i == 4764 || i == 4799 || i == 4852 || i == 4854 || i == 4889
     || i == 4942 || i == 4944 || i == 4979)
      continue;
    if (i == 5078 || i == 5168 || i == 5258 || i == 5348 || i == 5438
     || i == 5491 || i == 5493 || i == 5528 || i == 5581 || i == 5583
     || i == 5618 || i == 5671 || i == 5673 || i == 5708 || i == 5761
     || i == 5763 || i == 5798 || i == 5851 || i == 5853 || i == 5888
     || i == 5941 || i == 5943 || i == 5978 || i == 5993)
      continue;
    // ...
    if (i == 90048 || i == 90058 || i == 90064 || i == 90067 || i == 90068
     || i == 90079 || i == 90083 || i == 90128 || i == 90148 || i == 90156
     || i == 90158 || i == 90166 || i == 90168 || i == 90198 || i == 90238)
      continue;

    Bigint num(i);
    if (log)
      cout << num << ": ";
    int nbSteps = 0;
    bool abort = false;
    num = palindrome(num, nbSteps, abort);
    if (!abort && nbSteps > maxSteps)
    {
      maxSteps = nbSteps;
      maxNum = Bigint(i);
      maxPal = num;
      cout << "Biggest number of steps with " << maxNum << " nbSteps=" << maxSteps << " pal=" << maxPal << endl;
    }
    if (abort && saveAbort)
      abortList.push_back(Bigint(i));
    if (log)
      cout << num << " (nbSteps=" << nbSteps << ")" << endl;
  }
  if (saveAbort)
  {
    cout << "abort list:" << endl;
    for (auto ab : abortList)
      cout << ab << endl;
      //cout << ab << " || i == " ;
  }
}

Bigint Lychrel::palindrome(Bigint num, int& nbSteps, bool& abort)
{
  Bigint reverse = num.reverse();
  while ((num != reverse) && !abort)
  {
    nbSteps++;
    if (nbSteps > 300)
      abort = true;
    //cout << "nbSteps=" << nbSteps << " nbDigits=" << num.nbDigits() << endl;
    //cout << num << "+" << num.reverse() << endl;
    num = num + reverse;
    reverse = num.reverse();
  }
  return abort ? 0 : num;
}

void Lychrel::findFirstNumberGivenNbStep(const Bigint& start)
{
  int n = 100;
  int nbFound = 0;
  
  // initialize table
  vector<Bigint> v;
  v.insert(v.begin(), n, 0);
  for (size_t i = 0; i < lychrelTable.size(); ++i)
  {
    int nbSteps = get<2>(lychrelTable[i]);
    if (nbSteps > v.size())
      continue;
    
    v[nbSteps - 1] = Bigint(get<0>(lychrelTable[i]));
    nbFound++;
  }
  
  Bigint num = start;
  while (nbFound < n)
  {
    int nbSteps = 0;
    bool abort = false;
    Bigint p = Lychrel::palindrome(num, nbSteps, abort);
    if (!abort && nbSteps > 0 && nbSteps <= n && v[nbSteps - 1] == 0)
    {
      cout << " nbSteps=" << nbSteps << " " << num << " -> " << p << endl;
      v[nbSteps - 1] = num;
      nbFound++;
    }
    else
      num++;
  }
}

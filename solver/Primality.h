//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#pragma once

#include "Bigint.h"

enum class PrimalityStatus
{
  UNKNOWN,      // Not tested yet
  NOT_PRIME,    // Tested. Not prime.
  PRIME,        // Tested. Prime
  LIKELY_PRIME  // Tested. likely prime, but not 100% sure
};

static const Bigint two(2);

class Primality
{
public:
  static const vector<int> primesTable;
  
  // List n first primes
  static vector<int> firstPrimesTable(int n);
  // List primes smaller than n
  static vector<int> primesSmallerThan(int n);
  
  template <class T>
  static T firstFactor(T number);
  static bool isPrime(const Bigint& number);

  // Return greatest common divisor of a and b
  static Bigint GCD(const Bigint& a, const Bigint& b);
  
  // Primality test
  static PrimalityStatus MillerRabbin(const Bigint& n, int k);
  static PrimalityStatus MillerRabbinDeterministic(const Bigint& n);
  static bool MillerRabbinWitness(const Bigint& a, const Bigint& n);
  
  // Prime factorization
  static Bigint Pollard_p_minus_one(const Bigint& n, const Bigint& base, int m);
  static Bigint Pollard_Rho_Floyd(const Bigint&, const Bigint&);
  static Bigint Pollard_Rho_Brent(const Bigint&, const Bigint&);
 
  // return 1 if a is quadratic residu modulo n
  // return -1 otherwise
  static int JacobiSymbol(const Bigint& a, const Bigint& b);

  static PrimalityStatus SolovayStrassen(const Bigint& n, int k);
  
  static PrimalityStatus Williams_p_plus_one(int a, const Bigint& n);
  
  // Build Fermat number F_n = 2^2^n + 1
  //
  // If p divides F_n, p =  k * 2^{n + 1} + 1
  //
  // F_n = 0 [p] so 2^2^n = –1 [p]
  // so multiplicative order of 2 in ring Z/pZ is equal to 2^{n + 1}
  // this multiplicative order is a divisor of p – 1
  static Bigint FermatNumber(int n);
  
  // Mersenne primes
  // 2^p - 1 prime with p prime
  // n range is [1..22]
  static Bigint MersennePrimeNumber(int n);
  
  // Lucas sequence
  //
  // Fibonacci numbers are a specific Lucas sequence (Un with p = 1 and q = -1)
  // Lucas numbers are a specific Lucas sequence (Vn with p = 1 and q = -1)
  // Pell numbers are a specific Lucas sequence (Un with p = 2 and q = -1)
  static pair<Bigint, Bigint> LucasSequence(int n, int p, int q);
  
  static Bigint FibonacciNumber(int n);
  
  // For all even m < n, find 2 prime numbers p1 and p2 such as m = p1 + p2
  // If allSolutions is false, this method provides only one solution for each m
  static void goldbach(Bigint n, bool allSolutions);
  
  // Test PSW conjecture (Selfridge Pomerance Wagstaff)
  // If
  //   p is odd number ±2[5]
  //   2^(p-1) = 1[p]
  //   Fibonacci number F_(p+1) = 0[p]
  // Then p is prime
  static bool PSWconjecture(int start, int end, bool log = false);
  
  // List inverses in Z/nZ
  static void inversesZ_nZ(const Bigint& n);
};

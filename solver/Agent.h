//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#pragma once

#include "Bigint.h"

// An agent is a autonomous big number, able to move along autonomously

class Agent: public Bigint
{
public:
  static const int LENGTH_ALERT;
  
  enum Modus
  {
    MODUS1, //  3A+1
    MODUS2  // (3A+1)/2
  };

  Agent():Bigint(){}
  Agent(string s):Bigint(s){}
  Agent(int x):Bigint(x){}
  Agent(Bigint x):Bigint(x){}

  Agent& operator = (const Agent& copy);

  Agent& operator = (const Bigint& copy);

  // Spot the agent at a random place
  void spot();
  
  // Calcul flight delay
  // Example: 5->16->8->4->2->1 has flight delay = 5
  int flightDelay() const;
  
  // Display all steps of a given flight
  long flightDescription();

  // Let agent go and fly
  void goEven();

  // Let agent go and fly
  void goOdd();
  
  // Return Syracuse successor with original modus 3A+1
  void applyOriginalModus();
  
  void toast() const;

  vector<Agent> predecessors() const;
  
//private:
  //int length;
};

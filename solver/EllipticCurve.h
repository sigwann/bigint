//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#pragma once

#include "Bigint.h"
#include "ECpoint.h"

class EllipticCurve
{
  void SuyamaParametrisation(const Bigint&, const Bigint&, Bigint&, ECpoint&);
public:
  // Elliptic Curve Method - phase 1 only
  void ECM(const Bigint&, string chaine, const int, const int);
  void VerifSuyama(const Bigint&);
};

//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include <iostream>
#include "Bigint.h"
#include "Syracuse.h"
#include "Primality.h"
#include "Tests.h"
#include <math.h>
#include <assert.h>
#include <time.h>

// 2^31 - 1 = 2 147 483 647
// 2^63 - 1 = 9 223 372 036 854 775 807

int main(int argc, const char * argv[])
{
  assert(Bigint::LIMB_MAX < sqrt(INT_MAX)); // Reduce Bigint::LIMB_MAX
  assert(log10(Bigint::LIMB_MAX) == (int)log10(Bigint::LIMB_MAX)); // LIMB_MAX must be a power of BASE
  
  // Initialize rand
  srand((unsigned int)(time(NULL)));
  
  // Run tests
  //Tests::Test(Tests::TestsType::ALL);
  //Tests::Test(Tests::TestsType::PERFS);
  //Tests::Test(Tests::TestsType::STRESS);

  // Uncomment to run random Syracuse trials
  //Syracuse::randomTrials(1000);
  
  // Uncomment to find first class records
  // Syracuse s;
  // s.findClassRecords();
  // s.buildDelayClass(30);
  
  return 0;
}

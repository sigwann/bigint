//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#pragma once

#include <iostream>
#include <vector>
using namespace std;

class Bigint
{
public:
  static const int    BASE;
  static const int    MAXDIGITS;
  static const int    LIMB_MAX;
  static const int    KARATSUBA;
  // Thousands separator to display big integers.
  // Can also be "." for instance
  static const string SEPARATOR;

  vector<int> mLimbs;
  int8_t      mSign;
  bool        mNeedNormalization;
  
  // Constructors
  Bigint ();
  Bigint (string s) { parseString(s); }
  Bigint (int x);
  Bigint (const Bigint& copy);
  
  // Operators
  Bigint& operator = (const Bigint & copy);
  Bigint& operator = (const int);
  Bigint& operator = (string);
  Bigint operator + (const Bigint&) const;
  Bigint operator * (const Bigint&) const;
  Bigint operator * (const int) const;
  Bigint operator - (const Bigint&) const;
  Bigint operator ^ (int) const;           // Fast exponentation
  Bigint operator ^ (const Bigint&) const; // Fast exponentation
  Bigint& operator += (const Bigint&);
  Bigint& operator *= (const Bigint&);
  Bigint& operator *= (int);
  Bigint& operator -= (const Bigint&);
  bool operator > (int) const;
  bool operator > (const Bigint&) const;
  bool operator <= (const Bigint&) const;
  bool operator < (const Bigint&) const;
  bool operator >= (int) const;
  bool operator >= (const Bigint&) const;
  bool operator == (const Bigint&) const;
  bool operator == (int) const;           // Only for int < LIMB_MAX
  bool operator != (const Bigint&) const;
  bool operator != (int) const;           // Only for int < LIMB_MAX
  Bigint& operator %= (const Bigint&);
  Bigint operator % (const Bigint&) const;
  Bigint& operator - ();
  Bigint& operator ++ ();
  Bigint operator ++ (int);
  Bigint& operator -- ();
  Bigint operator -- (int);
  
  friend ostream& operator<<(ostream& os, const Bigint& t);
  
  void parseString(string s);
  // Return string of big integer with no space
  string toString() const;
  // Return string of big integer with separator every 3 digits
  string toStringFormated() const;
  
  inline size_t size() const { return mLimbs.size(); }
  int nbDigits() const;
  int sumOfDigits() const;
  // A = A div BASE
  void shiftRight();
  // A *= BASE
  void shiftLeft();
  // Return unit digit
  inline int unit() const { return mLimbs[0] % BASE; }
  // Return int value when possible
  int to_int() const;
  // Return true if even
  inline bool isEven() const { return mLimbs[0] % 2 == 0 ? true : false; }
  // Return absolute value
  Bigint abs() const;
  

  // Return number of 2 powers in this big int
  int nbTwoPower() const;
  // Divide this big int per 2
  void dividePerTwo();
  // Return c = A/t quotient entier via trial method
  // To be used only if big integer is a multiple of t
  Bigint exactDivision(const Bigint& t) const;
  // square root, technique du goutte a goutte : 1+3+5+....+(2n-1)=n≤
  Bigint squareRoot();
  // C = A x B via standard multiplication
  Bigint standardMultiplication(const Bigint&) const;
  // C = A x B via Karatsuba multiplication
  Bigint karatsubaMultiplication(const Bigint&) const;
  
  // Montgomery reduction
  // INPUT: T (this), R, R^{-1}, M, -M^{-1}
  // OUTPUT: T x R^{-1} [M]
  //
  // let M' = -M^{-1}
  // let U = T*M' [R]
  // We have (T + U*M) = T x R^{-1} [M]
  //
  // Explanations
  // U = T*M' + kR and M*M' = -1 + lR (Bezout theorem)
  // (T + U*M) = T + (T*M' + kR)*M = T + T(-1 + lR) + kR*M = lT*R + kM*R = (lT + kM)*R
  // So (T + U*M) / R is an integer
  Bigint MontgomeryReduction(const Bigint& R, const Bigint& R_inverse, const Bigint& Modulus, const Bigint& Modulus_opposite_inverse);
  // C = A x B mod M via Montgomery method
  Bigint mulMod(const Bigint&, const Bigint&, const int) const;
  // C = base^exponent mod M
  // Fast exponentiation
  Bigint exponentiationBySquaringModular(int exponent, const Bigint& m) const;
  // C = base^exponent mod M
  // Fast exponentiation
  Bigint exponentiationBySquaringModular(const Bigint& exponent, const Bigint& M) const;
  // Extended Euclide algorithm to calculate A' = A^(-1) mod b. We want A*A' - q*b = 1
  vector<Bigint> extendedEuclide(const Bigint& b) const;
  
  // Set a random big integer to this.
  // This will have between a and a + b limbs
  void randBI(int a, int b);
  // Return a random big integer lower than A
  Bigint randomValue() const;

  bool isPalindrome();
  Bigint reverse();
  
protected:
  // Make sure all limb >= 0 and < LIMB_MAX
  void normalize();
  // Input: b
  // Output: q and r
  // such as this = b * q + r
  void division(const Bigint& b, Bigint& q, Bigint& r) const;

private:
  inline void reset() { mSign = 1; mLimbs.clear(); }

  // Input: b
  // Output: this + b
  Bigint addition(const Bigint& b) const;
  // Input: b
  // Output: this - b
  Bigint subtraction(const Bigint& b) const;
  
  Bigint high(size_t) const;
  Bigint low(size_t) const;
};

//
// Big Integer library
// Created by Gregory Cousin
//
// This file is distributed under the MIT License. See the LICENSE
// file for details.
//

#include <iostream>

class Tests
{
  
public:
  enum class TestsType
  {
    ALL,
    PARSING,
    DISPLAY,
    DIGITS,
    OPERATORS,
    INEQUALITIES,
    STRESS,
    PRIMALITY,
    CRYPTO,
    PERFS,
    SYRACUSE
  };
  
  static bool Test(TestsType type);
private:
  static bool TestParsing();
  static bool TestDisplay();
  static bool TestDigits();
  static bool TestOperators();
  static bool TestInequalities();
  static bool TestStress();
  static bool TestPrimality();
  static bool TestCrypto();
  static bool TestPerfs();
  static bool TestSyracuse();
  
  static bool displayResult(bool isOK, const std::string& s = "");
  
  static uint32_t testIndex;
};
